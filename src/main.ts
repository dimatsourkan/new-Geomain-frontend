import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './App/app.module';
import { ENVIRONMENT } from './Environments/Environment';

import {hmrBootstrap} from "./App/hmr";

declare var module: { id: string };

if (ENVIRONMENT.PRODUCTION) {
  enableProdMode();
}

const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule);

if (ENVIRONMENT.HMR) {
    if (module[ 'hot' ]) {
        hmrBootstrap(module, bootstrap);
    }
    // else {
    //     console.error('HMR is not enabled for webpack-dev-server!');
    //     console.log('Are you using the --hmr flag for ng serve?');
    // }
} else {
    bootstrap();
}