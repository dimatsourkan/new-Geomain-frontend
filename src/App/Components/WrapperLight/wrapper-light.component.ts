import { Component } from '@angular/core';

@Component({
    selector: 'wrapper-light',
    templateUrl: './wrapper-light.component.html',
    styleUrls: ['./wrapper-light.component.less']
})

export class WrapperLightComponent {}
