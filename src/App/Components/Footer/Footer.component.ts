import {Component} from '@angular/core';
import {BaseComponent} from "../../BaseClasses/Components/Base.component";

@Component({
    selector: 'footer-component',
    templateUrl: './Footer.component.html',
    styleUrls: ['./Footer.component.less']
})

export class FooterComponent extends BaseComponent {}
