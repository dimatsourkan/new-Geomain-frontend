import { Component } from '@angular/core';
import { BaseComponent } from '../../BaseClasses/Components/Base.component';
import { Router } from '@angular/router';
import { DataStore } from '../../EntityModules/DataStore/data.store';
import { AuthorizationService } from '../../BaseModules/Authorization/Authorization.service';
import { IUser, USER_ROLE } from '../../EntityModules/User/User.model';



@Component({
  selector : 'header-component',
  templateUrl : './Header.component.html',
  styleUrls : ['./Header.component.less']
})

export class HeaderComponent extends BaseComponent {

  USER_ROLE = USER_ROLE;

  user : IUser;

  constructor(
    private router : Router,
    public authorizationService : AuthorizationService
  ) {
    super();
    this.user = DataStore.user.current.value;
    DataStore.user.current.asObservable().subscribe(user => this.user = user);
  }

  showAuthPopup() {
    this.router.navigate([{outlets : {auth : ['login']}}]);
  }
}
