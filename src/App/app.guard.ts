import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthorizationService } from './BaseModules/Authorization/Authorization.service';
import { UserService } from './EntityModules/User/Users.service';



@Injectable()
export class CanActivateApp implements CanActivate {

  constructor(
    private authorizationService : AuthorizationService,
    private userService : UserService
  ) {}

  canActivate() : Promise<boolean> {
    return new Promise((resolve) => {
      Promise.all([this.getUser()])
        .then(() => {
          return resolve(true);
        })
    });
  }

  getUser() {

    return new Promise((resolve) => {

      if(this.authorizationService.isAuthenticated) {

        return this.userService.getProfile().subscribe(res => {
          return resolve(true);
        }, err => {
          return resolve(true);
        });

      }

      return resolve(true);

    });
  }

}
