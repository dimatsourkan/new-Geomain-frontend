import { AfterViewInit, Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector : '[fixOnScroll]'
})
export class FixOnScrollDirective implements AfterViewInit {

  @Input() offsetTop : number = 0;
  initedPosition : number = 0;

  constructor(
    private elementRef : ElementRef,
    private renderer : Renderer2
  ) {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.initedPosition = this.topPosition - this.offsetTop;
      this.setStyles();
    }, 500);
  }

  get topPosition() {
    return document.documentElement.scrollTop + this.elementRef.nativeElement.getBoundingClientRect().top;
  }

  set positionStyle(position : number) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'transform', `translateY(${position}px)`);
  }

  setStyles() {
    if(document.documentElement.scrollTop >= this.initedPosition) {
      this.positionStyle = document.documentElement.scrollTop - this.initedPosition;
    } else {
      this.positionStyle = 0;
    }
  }

  @HostListener('document:scroll', ['$event.target'])
  public onScroll() {
    this.setStyles();
  }

}
