import { NgModule } from '@angular/core';
import { FixOnScrollDirective } from './FixOnScroll.directive';

@NgModule({
  imports : [],
  declarations : [
    FixOnScrollDirective
  ],
  exports : [
    FixOnScrollDirective
  ],
  providers : []
})
export class FixOnScrollModule {
}
