import {NgModule} from '@angular/core';
import {DonutChartDirective} from "./DonutChart.directive";



@NgModule({
    imports: [
    ],
    declarations: [
	    DonutChartDirective
    ],
    exports: [
	    DonutChartDirective
    ],
    providers: []
})
export class ChartModule{}
