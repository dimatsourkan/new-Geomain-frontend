import { Directive, ElementRef, Input, OnChanges, OnInit } from '@angular/core';



declare var Chart : any;


@Directive({
  selector : '[donutChart]'
})

export class DonutChartDirective implements OnInit, OnChanges {

  el : any;
  ctx : any;
  lineChart : any;

  @Input() data : any;

  constructor(private elem : ElementRef) {
    this.el = elem.nativeElement;
    this.ctx = this.el.getContext('2d');
  }

  options() {
    return {
      type : 'donut',
      data : {
        datasets : [{
          data : this.data,
          backgroundColor : [
            '#f2f5f7',
            '#ff0500'
          ]

        }],
        labels : [
          'Geomains',
          'Add-Ons'
        ]
      },
      options : {
        responsive : true
      }
    };
  }

  ngOnInit() {
    this.lineChart = new Chart(this.ctx, this.options());
  }

  ngOnChanges() {

    if(this.lineChart) {
      this.lineChart.data.datasets[0].data = this.data;
      this.lineChart.update();
    }
  }

}
