import { ActivatedRouteSnapshot, CanActivate, CanLoad, Router } from '@angular/router';
import { AuthorizationService } from '../Authorization.service';
import { Injectable } from '@angular/core';
import { Route } from '@angular/router/src/config';
import { DataStore } from '../../../EntityModules/DataStore/data.store';



@Injectable({
  providedIn : 'root'
})
export class IsAuthenticated implements CanActivate, CanLoad {

  constructor(private AS : AuthorizationService,
              private Router : Router) {

  }

  canLoad(route : Route) : Promise<boolean> {
    return this.checkActivate(route);
  }

  canActivate(route : ActivatedRouteSnapshot) : Promise<boolean> {
    return this.checkActivate(route);
  }

  checkActivate(route : Route | ActivatedRouteSnapshot) : Promise<boolean> {

    return new Promise((resolve) => {

      if(this.AS.isAuthenticated) {

        if(DataStore.user.current) {

          if(!route.data) {
            return resolve(true);
          }

          if(this.AS.fromRoles(route.data['roles'])) {
            return resolve(true);
          }
          else {
            this.Router.navigate(['/home']);
            return resolve(false);
          }

        }
        else {

          this.AS.logout();
          this.Router.navigate(['/home']);
          return resolve(false);

        }
      }

      this.Router.navigate(['/home']);
      return resolve(false);
    });
  }

}