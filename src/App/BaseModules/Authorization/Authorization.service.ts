import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenService } from '../../BaseClasses/Services/Token.service';
import 'rxjs/add/operator/map';
import { User } from '../../EntityModules/User/User.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ENVIRONMENT } from '../../../Environments/Environment';
import { map } from 'rxjs/internal/operators';
import { DataStore } from '../../EntityModules/DataStore/data.store';
import { Router } from '@angular/router';



@Injectable({
  providedIn : 'root'
})
export class AuthorizationService {

  BASE_URL = ENVIRONMENT.AUTH_URL;

  /**
   * Хедеры для отправки на сервер
   */
  headers : HttpHeaders;

  constructor(private http : HttpClient,
              private token : TokenService,
              private router : Router) {

    this.headers = new HttpHeaders({
      'Authorization' : `Basic ${btoa(ENVIRONMENT.CREDENTIALS)}`
    });
  }

  /**
   * Авторизация пользователя
   * @param {string} username
   * @param {string} password
   * @param {boolean} rememberMe
   * @returns {Observable<>}
   */
  login(username : string, password : string, rememberMe : boolean = false) : Observable<any> {
    return this.http
      .post(
        `${this.BASE_URL}/oauth/token`,
        {},
        {headers : this.headers, params : {username, password, grant_type : 'password'}}
      ).pipe(
        map<any, any>((res) => this.setLogin(res))
      );

  }

  confirm(token : string) : Observable<any> {
    return this.http
      .get(`${this.BASE_URL}/auth/verification/${token}`, {headers : this.headers});
  }

  forgotPass(login : string) : Observable<any> {
    return this.http
      .get(`${this.BASE_URL}/api/open/user/forgot-password`, {headers : this.headers, params : {login}});
  }

  changePass(password : string, token : string) : Observable<any> {
    return this.http
      .post(`${this.BASE_URL}/api/open/user/update-password`, {password, token}, {headers : this.headers});
  }

  confirmPass(password : string, token : string) : Observable<any> {
    return this.http
      .post(`${this.BASE_URL}/auth/password-confirm`, {password, token}, {headers : this.headers});
  }

  registrarSignUp(formData : any) : Observable<any> {
    return this.http
      .post(`${this.BASE_URL}/auth/registrar-signup`, formData, {headers : this.headers});
  }

  setLogin(res : any) {
    /** Установка токена **/
    this.token.setTokens(res.access_token, res.refresh_token);
    /** Возвращаем результат **/
    return res;
  }

  /**
   * Логаут - требуется только вызвать этот метод
   * Токен для идентификации подставляется сам
   * @returns {Observable<Response>}
   */
  logout() {
    DataStore.clearData();
    this.token.resetToken();
    this.router.navigate(['/']);
    // document.location.reload();
  }

  /**
   * Проверяет авторизован ли пользователь по наличию токена в локальном хранилище
   * @returns {Promise<boolean>}
   */
  get isAuthenticated() : boolean {
    return !!this.token.getToken();
  }

  fromRoles(roles : string[]) {

    if(this.isAuthenticated && DataStore.user.current.value) {

      if(roles.indexOf('*') >= 0) {
        return true;
      }

      return roles.indexOf(DataStore.user.current.value.role) >= 0;
    }
    else {
      return false;
    }

  }

}