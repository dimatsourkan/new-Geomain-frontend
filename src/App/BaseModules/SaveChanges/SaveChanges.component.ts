import { Component, EventEmitter, Output } from '@angular/core';
import { BaseComponent } from '../../BaseClasses/Components/Base.component';



@Component({
  selector : 'save-changes',
  templateUrl : './SaveChanges.component.html',
  styleUrls : ['./SaveChanges.component.less']
})

export class SaveChangesComponent extends BaseComponent {
  @Output() public onClick = new EventEmitter<any>();
}
