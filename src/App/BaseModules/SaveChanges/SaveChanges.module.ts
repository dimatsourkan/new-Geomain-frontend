import { SaveChangesComponent } from './SaveChanges.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  imports : [
    CommonModule
  ],
  declarations : [
    SaveChangesComponent
  ],
  exports : [
    SaveChangesComponent
  ],
  providers : []
})

export class SaveChangesModule {
}
