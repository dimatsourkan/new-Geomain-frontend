import { Component, Input } from '@angular/core';



@Component({
  selector : 'validation-message',
  templateUrl : './Validation.component.html',
  styleUrls : ['./Validation.component.less']
})

export class ValidationComponent {

  @Input() public errors : any;

}
