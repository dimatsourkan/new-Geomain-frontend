import { Component, Input } from '@angular/core';
import { BaseComponent } from '../../BaseClasses/Components/Base.component';
import * as $ from 'jquery';



@Component({
  selector : 'scroll-to-top',
  templateUrl : './ScrollToTop.component.html',
  styleUrls : ['./ScrollToTop.component.less']
})

export class ScrollToTopComponent extends BaseComponent {

  @Input() position : string; //right-bottom,

  toTop() {
    $('html, body').animate({
      scrollTop : 0
    }, 300);
  }

}
