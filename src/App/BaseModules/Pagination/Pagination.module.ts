import { PaginationComponent } from './Pagination.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  imports : [
    CommonModule
  ],
  declarations : [
    PaginationComponent
  ],
  exports : [
    PaginationComponent
  ],
  providers : []
})

export class PaginationModule {
}
