import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Pagination } from '../../BaseClasses/Models/Pagination.model';


@Component({
  selector : 'pagination',
  templateUrl : './Pagination.component.html',
  styleUrls : ['./Pagination.component.less']
})

export class PaginationComponent {
  @Input() showed : number = 7;
  @Input() pagination : Pagination = new Pagination();
  @Output() onChange : EventEmitter<number> = new EventEmitter<number>();

  showedPages : number[];

  changePage(page : number) {
    this.onChange.emit(page);
  }

  ngOnChanges() {
    this.showedPages = this.getPaginationPages();
  }

  getPaginationPages() : number[] {
    let pagesArray : number[] = [];
    let firstPage  : number   = this.getFirstPage();
    let lastPage   : number   = this.getLastPage(firstPage);

    for(let i = firstPage; i <= lastPage; i++) {
      pagesArray.push(i);
    }

    if(pagesArray.length > this.showed) {
      pagesArray = pagesArray.slice(0 , this.showed);
    }

    if(pagesArray.length <= 0) {
      pagesArray.push(this.pagination.page);
    }

    return pagesArray;
  }

  /**
   * Получает номер первой страницы в списке
   * @returns {number}
   */
  private getFirstPage() : number {
    let firstPage : number = Math.ceil(this.pagination.page - this.showed/2);
    if(firstPage < 1) {
      firstPage = 1;
    }

    return firstPage;
  }

  /**
   * Получает последнюю страницу в списке
   * @param firstPage - ПЕрвая страница в списке
   * @returns {number}
   */
  private getLastPage(firstPage : number) : number {

    let lastPage : number   = Math.floor(this.pagination.page + this.showed/2);

    if(firstPage == 1) {
      lastPage = this.pagination.page + this.showed;
    }

    let total = this.pagination.totalPages;
    if(lastPage > total) {
      lastPage = total;
    }

    return lastPage;
  }
}
