import { AfterViewInit, Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';
import * as $ from 'jquery';
import * as Inputmask from 'inputmask/dist/inputmask/inputmask';
import 'inputmask/dist/inputmask/inputmask.date.extensions';
// import 'inputmask/dist/inputmask/inputmask.extensions';



@Component({
  selector : 'input-text',
  template : `
      <label class="form-label"
         [ngClass]="{'is-not-empty' : value, 'form-error' : form?.controls[formControlName].errors}">
          <input #input
                 class="form-control {{ customClass }}"
                 [attr.maxlength]="maxlength"
                 [placeholder]="placeholder"
                 [disabled]="disabled ? true : null"
                 (input)="propagateChange($event.target.value)"
                 (change)="toChange($event.target.value)"
                 (keypress)="onKeyPress($event)">
          <span class="form-control-title">
                <ng-content></ng-content>
                <span *ngIf="reqField">*</span>
            </span>
      </label>

      <div *ngIf="form?.controls[formControlName]">
          <validation-message [errors]="form?.controls[formControlName].errors">
          </validation-message>
      </div>

  `,
  styles : [''],
  styleUrls : ['../FormControls.component.less'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => InputTextComponent),
      multi : true
    }
  ]
})

export class InputTextComponent extends FormControlsComponent implements OnInit, AfterViewInit {

  @Input() mask : string = null;

  @Input() regexp : string = null;

  @Input() maxlength : string = null;

  @Input() options : any = null;

  @ViewChild('input') private input : any;

  private _options : any = {
    placeholder : '_'
  };

  ngOnInit() {
    this.input.nativeElement.value = this.value;
  }

  ngAfterViewInit() {

    let options : any = $.extend(this._options, this.options);

    if(this.mask) {
      Inputmask(this.mask, options).mask(this.input.nativeElement);
    }

    if(options.alias) {
      Inputmask(options).mask(this.input.nativeElement);
    }
  }

  writeValue(value : any) {
    super.writeValue(value);
    this.input.nativeElement.value = value;
  }

  onKeyPress(target : any) {

    if(!this.regexp) {
      return true;
    }

    if(!String.fromCharCode(target.which).match(this.regexp)) {
      return false;
    }

  }

}

