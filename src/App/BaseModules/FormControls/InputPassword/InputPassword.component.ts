import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';
import { Helpers } from '../../../BaseClasses/Helpers';



@Component({
  selector : 'password',
  template : `

    <label class="form-label"
     [ngClass]="{'is-not-empty' : value, 'form-error' : form?.controls[formControlName].errors}">
      <input class="form-control"
             type="password"
             [value]="value"
             [attr.maxlength]="maxlength"
             [placeholder]="placeholder"
             [disabled]="disabled ? true : null"
             (input)="propagateChange($event.target.value)"
             (change)="toChange($event.target.value)">
      <span class="form-control-title"><ng-content></ng-content></span>
    </label>


    <div *ngIf="form?.controls[formControlName]">
      <validation-message [errors]="form?.controls[formControlName].errors">
      </validation-message>
    </div>

  `,
  styleUrls : [
    '../FormControls.component.less'
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => InputPasswordComponent),
      multi : true
    }
  ]
})

export class InputPasswordComponent extends FormControlsComponent {

  helpers : Helpers = new Helpers();

  @Input() maxlength : number = 999;

}
