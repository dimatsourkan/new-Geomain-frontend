import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';



@Component({
  selector : 'radio',
  template : `
      <label class="radio {{ addClass }}"
             [ngClass]="disabled && !checked ? 'disabled': disabled && checked ? 'disabled-checked': null">
          <input type="radio" [checked]="checked"
                 name="{{ radioname }}" [value]="value"
                 (change)="propagateChange($event.target.value)"
                 [disabled]="disabled ? true : null">
          <i></i>
          <ng-content></ng-content>
      </label>

      <div *ngIf="form?.controls[formControlName]">
          <validation-message [errors]="form?.controls[formControlName].errors"></validation-message>
      </div>
  `,
  styleUrls : [`./Radiobutton.component.less`],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => RadioComponent),
      multi : true
    }
  ]
})

export class RadioComponent extends FormControlsComponent {

  @Input()
  radioname : string = '';

  @Input()
  checked : boolean = false;

  @Input()
  disabled : boolean = false;

  @Input()
  addClass : string = '';

  writeValue(value : any) {

    if(value === this.value) {
      this.checked = true;
    }
  }

}
