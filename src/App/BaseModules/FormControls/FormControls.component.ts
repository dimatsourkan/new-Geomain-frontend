import { Input } from '@angular/core';
import { ControlValueAccessor, FormGroup } from '@angular/forms';



export class FormControlsComponent implements ControlValueAccessor {

  @Input()
  name : any;

  @Input()
  customClass : string = '';

  @Input()
  nested : string;

  @Input()
  reqField : boolean = false;

  @Input()
  formControlName : any;

  @Input()
  placeholder : string = ' ';

  @Input()
  value : any = null;

  @Input()
  form : FormGroup;

  @Input()
  disabled : boolean = false;

  writeValue(value : any) {
    this.value = value || null;
  }

  propagateChange = (_ : any) => {
    this.value = _;
  };

  registerOnChange(fn : any) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  toChange(value : any) {
    this.value = value || null;
  }
}
