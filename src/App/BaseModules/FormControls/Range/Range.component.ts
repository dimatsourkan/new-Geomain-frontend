import { AfterViewInit, Component, ElementRef, forwardRef, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';
declare var $ : any;


@Component({
  selector : 'range',
  template : `
    <div class="my-range-slider">
      <input type="text" #input>
    </div>
  `,
  encapsulation : ViewEncapsulation.None,
  styleUrls : [`./Range.component.less`],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => RangeComponent),
      multi : true
    }
  ]
})

export class RangeComponent extends FormControlsComponent implements AfterViewInit {

  @ViewChild('input') private input : ElementRef;

  @Input() options : any;

  private _options = {

  };

  private $slider : any;

  ngAfterViewInit() {

    let options = $.extend(this._options, this.options);

    options.type = 'single';
    options.from = this.value;
    options.hide_min_max = true;
    options.hide_from_to = true;
    options.onChange = (data : any) => {
      this.propagateChange(data.from);
    };

    this.$slider = $(this.input.nativeElement).ionRangeSlider(options).data("ionRangeSlider");
  }

  writeValue(value : any) {
    super.writeValue(value);

    console.log(value);

    if(this.$slider) {
      this.$slider.update({
        from : value
      });
    }
  }

}
