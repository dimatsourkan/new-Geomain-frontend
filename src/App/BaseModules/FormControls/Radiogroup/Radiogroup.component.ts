import { AfterViewInit, Component, forwardRef, Input, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { FormControlsComponent } from '../FormControls.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';



@Component({
  selector : 'radiogroup',
  template : `
      <div #radiogroup class="radiogroup">
          <div class="radiogroup-title" *ngIf="name">{{ name }}</div>
          <ng-content></ng-content>
      </div>
  `,
  styles : [`
    .radiogroup {
      padding: 14px 20px;
    }
    .radiogroup-title {
      font-size: 11px;
      color: #606d70;
      margin-bottom: 7px;
      font-weight: 600;
    }
  `],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => RadiogroupComponent),
      multi : true
    }
  ]
})

export class RadiogroupComponent extends FormControlsComponent implements AfterViewInit {

  @ViewChild('radiogroup') private radiogroup : any;

  ngAfterViewInit() {
    $(this.radiogroup.nativeElement).find('input[type="radio"]').change((e) => {
      this.propagateChange(e.target.value);
    });
  }

}
