import { Component, forwardRef, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';



@Component({
  selector : 'search',
  template : `
      <label class="form-label"
         [ngClass]="{'is-not-empty' : value, 'form-error' : form?.controls[formControlName].errors}">
          <input #input
                 class="form-control-search"
                 [attr.maxlength]="maxlength"
                 [placeholder]="placeholder"
                 [disabled]="disabled ? true : null"
                 (input)="propagateChange($event.target.value)"
                 (change)="toChange($event.target.value)"
                 (keypress)="onKeyPress($event)">
      </label>

  `,
  styles : [''],
  styleUrls : ['../FormControls.component.less'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => InputSearchComponent),
      multi : true
    }
  ]
})

export class InputSearchComponent extends FormControlsComponent {

  @ViewChild('input') private input : any;

  @Input() maxlength : string = null;

  @Input() regexp : string = null;

  ngOnInit() {
    this.input.nativeElement.value = this.value;
  }

  writeValue(value : any) {
    super.writeValue(value);
    this.input.nativeElement.value = value;
  }

  onKeyPress(target : any) {

    if(!this.regexp) {
      return true;
    }

    if(!String.fromCharCode(target.which).match(this.regexp)) {
      return false;
    }

  }

}

