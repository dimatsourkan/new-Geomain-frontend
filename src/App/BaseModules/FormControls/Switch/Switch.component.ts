import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';
import { Helpers } from '../../../BaseClasses/Helpers';



type type = 'red';


@Component({
  selector : 'my-switch',
  template : `
      <label class="switch">
          <input type="checkbox" [checked]="checked || value"
                 (change)="propagateChange($event.target.checked)" [disabled]="disabled ? true : null">
          <span class="switch-item"></span>
          <ng-content></ng-content>
      </label>

      <div *ngIf="form?.controls[formControlName]">
          <validation-message [errors]="form?.controls[formControlName].errors"></validation-message>
      </div>
  `,
  styleUrls : [`./Switch.component.less`],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => SwitchComponent),
      multi : true
    }
  ]
})

export class SwitchComponent extends FormControlsComponent {

  helpers : Helpers = new Helpers();

  @Input()
  checked : boolean = false;

  @Input()
  disabled : boolean = false;

  @Input()
  type : type = null;

  propagateChange = (_ : any) => {
    this.checked = _;
    this.value = _;
  };
}
