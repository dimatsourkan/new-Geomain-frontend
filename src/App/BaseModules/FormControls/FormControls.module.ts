import { NgModule } from '@angular/core';
import { InputTextComponent } from './InputText/InputText.component';
import { ValidationModule } from '../Validation/Validation.module';
import { CommonModule } from '@angular/common';
import { CheckboxComponent } from './Checkbox/Checkbox.component';
import { RadioComponent } from './Radiobutton/Radiobutton.component';
import { InputPasswordComponent } from './InputPassword/InputPassword.component';
import { FormsModule } from '@angular/forms';
import { RadiogroupComponent } from './Radiogroup/Radiogroup.component';
import { InputTextAreaComponent } from './InputTextArea/InputTextArea.component';
import { SwitchComponent } from './Switch/Switch.component';
import { ASelectComponent } from './ASelect/ASelect.component';
import { InputSearchComponent } from './InputSearch/InputText.component';
import { RangeComponent } from './Range/Range.component';
import { ClickOutsideModule } from '../ClickOutside/ClickOutside.module';



@NgModule({
  imports : [
    CommonModule,
    ValidationModule,
    ClickOutsideModule,
    FormsModule
  ],
  declarations : [
    RadioComponent,
    SwitchComponent,
    CheckboxComponent,
    InputTextComponent,
    InputTextAreaComponent,
    RadiogroupComponent,
    ASelectComponent,
    RangeComponent,
    InputSearchComponent,
    InputPasswordComponent
  ],
  exports : [
    RadioComponent,
    SwitchComponent,
    CheckboxComponent,
    InputTextComponent,
    InputTextAreaComponent,
    RadiogroupComponent,
    ASelectComponent,
    RangeComponent,
    InputSearchComponent,
    InputPasswordComponent
  ]

})

export class FormControlsModule {
}

