import { Component, forwardRef, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';
import * as Autosize from 'autosize/dist/autosize.js';
// import * as $ from 'jquery';
// import * as Inputmask from 'inputmask/dist/inputmask/inputmask';
// import 'inputmask/dist/inputmask/inputmask.extensions';



@Component({
  selector : 'input-textarea',
  template : `
    <label class="form-label"
           [ngClass]="{'is-not-empty' : value, 'form-error' : form?.controls[formControlName].errors}">
            <textarea #input
                      class="form-control"
                      [attr.maxlength]="maxlength"
                      [placeholder]="placeholder"
                      [disabled]="disabled ? true : null"
                      (input)="propagateChange($event.target.value)"
                      (change)="toChange($event.target.value)"></textarea>
      <span class="form-control-title">
                <ng-content></ng-content>
            </span>
    </label>

    <div *ngIf="form?.controls[formControlName]">
      <validation-message [errors]="form?.controls[formControlName].errors"></validation-message>
      <div *ngIf="form?.controls[formControlName]"></div>
    </div>

  `,
  styles : [''],
  styleUrls : ['../FormControls.component.less'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => InputTextAreaComponent),
      multi : true
    }
  ]
})

export class InputTextAreaComponent extends FormControlsComponent {

  @Input() mask : string = null;

  @Input() type : string = 'text';

  @Input() maxlength : string = null;

  @Input() options : any = null;

  @ViewChild('input') private input : any;

  private _options : any = {
    placeholder : '_'
  };

  writeValue(value : any) {
    super.writeValue(value);
    this.input.nativeElement.value = value;
    Autosize.update(this.input.nativeElement);
  }

  ngOnInit() {
    this.input.nativeElement.value = this.value;
  }

  ngAfterViewInit() {

    Autosize(this.input.nativeElement);

    // let options : any = $.extend(this._options, this.options);
    //
    // if(this.mask) {
    //   Inputmask(this.mask, options).mask(this.input.nativeElement);
    // }
    //
    // if(options.alias) {
    //   Inputmask(options).mask(this.input.nativeElement);
    // }
  }


}
