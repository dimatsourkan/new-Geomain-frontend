import {
  AfterViewInit, Component, ElementRef, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output,
  ViewChild
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';
import * as $ from 'jquery';
import 'inputmask/dist/inputmask/inputmask.extensions';


type TO_TOP = 'to-top';

@Component({
  selector : 'a-select',
  template : `
      <div class="relative {{ position }}" (appClickOutside)="hideDropdown()">
        
        <label class="form-label form-select-label"
               (click)="toggleDrodown()"
               [ngClass]="{
                 'is-not-empty' : value, 
                 'form-error' : form?.controls[formControlName].errors,
                 'is-disabled' : disabled
               }">
          <input #input
                 class="form-control"
                 [disabled]="true">
          <span class="form-control-title">
            {{ placeholder }}
            <span *ngIf="reqField">*</span>
        </span>
        </label>

        <div class="select-dropdown" #dropdown [ngClass]="{ hidden : !dropShowed }">
          <ng-content select="[a-select-option]"></ng-content>
          <ng-content select="[a-select-option-disabled]"></ng-content>
        </div>
        
      </div>

      <div *ngIf="form?.controls[formControlName]">
          <validation-message [errors]="form?.controls[formControlName].errors">
          </validation-message>
      </div>

  `,
  styleUrls : ['../FormControls.component.less'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => ASelectComponent),
      multi : true
    }
  ]
})

export class ASelectComponent extends FormControlsComponent implements OnInit, OnChanges {

  @Input() position : TO_TOP;

  @ViewChild('input') private input : ElementRef;
  @ViewChild('dropdown') private dropdown : ElementRef;

  @Output('change') private change : EventEmitter<any> = new EventEmitter<any>();

  dropShowed : boolean = false;

  ngOnInit() {

    let $drop = $(this.dropdown.nativeElement);

    this.input.nativeElement.value = this.value;

    $drop.on('click', '> div[a-select-option]', e => {

      $drop.find('> div').removeClass('selected');
      this.toggleDrodown();
      this.writeValue($(e.currentTarget).attr('value'));
      this.change.emit(this.value);

    });

  }

  ngOnChanges() {
    this.input.nativeElement.value = this.value;
  }

  selectItem($item : any) {

    $item.addClass('selected');
    this.input.nativeElement.value = $item.find('[a-select-text]').text();
    this.propagateChange($item.attr('value'));

  }

  writeValue(value : any) {

    super.writeValue(value);

    if(value) {
      this.selectItem($(this.dropdown.nativeElement).find(`> div[a-select-option][value="${value}"]`));
    }

  }

  toggleDrodown() {

    if(this.disabled) {
      return;
    }

    this.dropShowed = !this.dropShowed;
  }

  hideDropdown() {
    this.dropShowed = false;
  }

}

