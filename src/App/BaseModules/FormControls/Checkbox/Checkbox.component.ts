import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../FormControls.component';
import { Helpers } from '../../../BaseClasses/Helpers';



type type = 'red';


@Component({
  selector : 'checkbox',
  template : `
      <label class="checkbox"
             [ngClass]="{
                    'disabled'          : disabled && !checked,
                    'disabled-checked'  : disabled && checked
                }">
          <input type="checkbox" [checked]="checked || value"
                 (change)="propagateChange($event.target.checked)" [disabled]="disabled ? true : null">
          <i></i>
          <ng-content></ng-content>
      </label>

      <div *ngIf="form?.controls[formControlName]">
          <validation-message [errors]="form?.controls[formControlName].errors"></validation-message>
      </div>
  `,
  styleUrls : [`./Checkbox.component.less`],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => CheckboxComponent),
      multi : true
    }
  ]
})

export class CheckboxComponent extends FormControlsComponent {

  helpers : Helpers = new Helpers();

  @Input()
  checked : boolean = false;

  @Input()
  disabled : boolean = false;
}
