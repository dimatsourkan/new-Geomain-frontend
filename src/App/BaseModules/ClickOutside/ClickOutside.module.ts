import { NgModule } from '@angular/core';
import { ClickOutsideDirective } from './ClickOutside.directive';

@NgModule({
  imports : [],
  declarations : [
    ClickOutsideDirective
  ],
  exports : [
    ClickOutsideDirective
  ],
  providers : []
})
export class ClickOutsideModule {
}
