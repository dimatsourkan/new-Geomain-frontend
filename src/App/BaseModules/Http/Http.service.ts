import { Injectable } from '@angular/core';
import { TokenService } from '../../BaseClasses/Services/Token.service';
import { HttpClient, HttpErrorResponse, HttpHandler, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, ObservableInput } from 'rxjs/Observable';
import { HttpErrorService } from '../HttpError/HttpError.service';



@Injectable()
export class AppHttpService extends HttpClient {

  protected token : TokenService;

  constructor(HttpHandler : HttpHandler,
              private HttpErrorService : HttpErrorService) {
    super(HttpHandler);

    this.token = new TokenService();
  }

  /**
   * Функция добавляет хередеры перед отправкой запроса
   * @param options
   */
  private modifyHeaders(options : any = {}) {

    if(!options) {
      options = {};
    }

    let headers = {
      'X-Requested-With' : 'XMLHttpRequest'
    };

    if(options.headers.get('Authorization')) {
      headers['Authorization'] = options.headers.get('Authorization');
    }
    else {
      if(this.token.getToken()) {
        headers['Authorization'] = `Bearer ${this.token.getToken()}`;
      }
    }

    options.headers = new HttpHeaders(headers);

    return options;
  }


  get(url : string, options? : any) : Observable<any> {

    options = this.modifyHeaders(options);
    return super.get(url, options).pipe(
      catchError<Observable<ArrayBuffer>, Observable<any>>((e) => this.errorHandler(e))
    );
  }

  post(url : string, body : any, options? : any) : Observable<any> {
    options = this.modifyHeaders(options);
    return super.post(url, body, options).pipe(
      catchError<Observable<ArrayBuffer>, Observable<any>>((e) => this.errorHandler(e))
    );
  }

  put(url : string, body : any, options? : any) : Observable<any> {
    options = this.modifyHeaders(options);
    return super.put(url, body, options).pipe(
      catchError<Observable<ArrayBuffer>, Observable<any>>((e) => this.errorHandler(e))
    );
  }

  delete(url : string, options? : any) : Observable<any> {
    options = this.modifyHeaders(options);
    return super.delete(url, options).pipe(
      catchError<Observable<ArrayBuffer>, Observable<any>>((e) => this.errorHandler(e))
    );
  }

  private errorHandler(error : HttpErrorResponse) : ObservableInput<Observable<any>> {
    this.catchErrors(error);
    throw(error.error);
  }

  catchErrors(error : HttpErrorResponse) {
    this.HttpErrorService.setError(error);
  }
}
