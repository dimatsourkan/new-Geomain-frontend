import { NgModule } from '@angular/core';
import { AppHttpFactory } from './Http.facory';
import { HttpErrorModule } from '../HttpError/HttpError.module';
import { HttpErrorService } from '../HttpError/HttpError.service';
import { HttpClient, HttpHandler } from '@angular/common/http';



@NgModule({
  imports : [
    HttpErrorModule
  ],
  providers : [
    {
      provide : HttpClient,
      useFactory : AppHttpFactory,
      deps : [HttpHandler, HttpErrorService]
    }
  ]
})
export class AppHttpModule {
}
