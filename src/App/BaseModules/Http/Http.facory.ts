import { AppHttpService } from './Http.service';
import { HttpHandler } from '@angular/common/http';
import { HttpErrorService } from '../HttpError/HttpError.service';



export function AppHttpFactory(HttpHandler : HttpHandler, HttpErrorService : HttpErrorService) {
  return new AppHttpService(HttpHandler, HttpErrorService);
}
