import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';



@Component({
  selector : 'confirm',
  templateUrl : 'Confirm.component.html',
  styleUrls : ['Confirm.component.less']
})

export class ConfirmModal {

  @Output('onSuccessEvent') onSuccessEvent : EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('modal') private modal : any;

  @Input() error : string = '';

  onSuccess : Function = () => {};

  open(callback = () => {}) {
    this.modal.open();
    this.onSuccess = callback;
  }

  close() {
    this.modal.close();
  }

}
