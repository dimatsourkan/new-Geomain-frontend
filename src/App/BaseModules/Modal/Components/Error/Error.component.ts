import { Component, Input, ViewChild } from '@angular/core';
import { Helpers } from '../../../../BaseClasses/Helpers';



@Component({
  selector : 'error',
  templateUrl : 'Error.component.html',
  styleUrls : ['Error.component.less']
})

export class ErrorModal {

  @ViewChild('modal') private modal : any;

  helpers : Helpers = new Helpers();


  open() {
    this.modal.open();
  }

  close() {
    this.modal.close();
  }

}
