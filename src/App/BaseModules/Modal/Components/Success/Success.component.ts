import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { Helpers } from '../../../../BaseClasses/Helpers';
import { Modal } from '../Modal/Modal.component';



@Component({
  selector : 'success',
  templateUrl : 'Success.component.html',
  styleUrls : ['Success.component.less']
})

export class SuccessModal {

  @Output('onClose') public onClose : EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('modal') private modal : Modal;

  helpers : Helpers = new Helpers();

  open() {
    this.modal.open();
  }

  close() {
    this.modal.close();
  }

}
