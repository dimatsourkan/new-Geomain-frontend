import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import * as jQuery from 'jquery';
import '../../../../../../node_modules/remodal/src/remodal.js';



@Component({
  selector : 'modal',
  templateUrl : 'Modal.component.html',
  styleUrls : ['Modal.component.less']
})

export class Modal implements AfterViewInit, OnDestroy {

  @ViewChild('modal') protected modal : any;

  @Input('options') protected options : any = {};

  @Input('customClass') public customClass : string = '';

  @Output('onClose') protected onClose : EventEmitter<any> = new EventEmitter<any>();

  private $modal : any;

  open() {
    this.$modal.open();
  }

  close() {
    this.$modal.close();
  }

  ngAfterViewInit() {
    let $el : any = jQuery(this.modal.nativeElement);
    this.$modal = $el.remodal(this.options);
    jQuery(this.$modal.$modal).on('closed', () => {
      this.onClose.emit();
    });
  }

  ngOnDestroy() {
    this.$modal.destroy();
  }

}
