import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Modal } from './Components/Modal/Modal.component';
import { ErrorModal } from './Components/Error/Error.component';
import { ConfirmModal } from './Components/Confirm/Confirm.component';
import { SuccessModal } from './Components/Success/Success.component';



@NgModule({
  imports : [
    CommonModule
  ],
  declarations : [
    Modal,
    ErrorModal,
    ConfirmModal,
    SuccessModal
  ],
  exports : [
    Modal,
    ErrorModal,
    ConfirmModal,
    SuccessModal
  ]

})
export class ModalModule {
}

