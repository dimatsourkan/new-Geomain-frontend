import { Component } from '@angular/core';
import { NotificationsService } from '../../Notifications.service';



@Component({
  selector : 'notification-list',
  templateUrl : './List.component.html',
  styles : [
      `
      .toast-container {
        max-height: 100%;
        overflow: auto;
        padding: 10px 10px;
        z-index: 10001 !important;
      }
    `
  ]
})

export class NotificationsComponent {

  constructor(public NotificationsService : NotificationsService) {
  }

  remove(index : number) {
    this.NotificationsService.removeByIndex(index);
  }
}
