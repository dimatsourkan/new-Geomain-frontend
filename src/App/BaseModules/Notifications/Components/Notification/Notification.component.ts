import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Notification } from '../../Notifications.model';



@Component({
  selector : 'notification',
  styles : [
      `
          .toast-title {
              margin-bottom: 10px;
          }
    `
  ],
  templateUrl : './Notification.component.html'
})

export class NotificationComponent implements OnInit {
  @Input() notification : Notification;
  @Input() timeout : number;

  @Output() onRemove : EventEmitter<any> = new EventEmitter<any>();

  ngOnInit() {
    if(this.timeout) {
      setTimeout(() => this.remove(), this.timeout);
    }
  }

  remove() {
    this.onRemove.emit();
  }
}
