import { DataStore } from '../EntityModules/DataStore/data.store';

declare var require : any;


export interface IHelpers {
  img(image : string);
  isMine(id : number);
}


export class Helpers {

  static img(image : string) {
    return require('./../../Theme/Img/' + image);
  }

  static isMine(id : number) {

    if(!DataStore.user.current.value) {
      return false;
    }

    if((!DataStore.user.current.value.id || !id)) {
      return false;
    }

    return id === DataStore.user.current.value.id;

  }

}
