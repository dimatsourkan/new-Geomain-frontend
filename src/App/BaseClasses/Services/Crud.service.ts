import { Injectable } from '@angular/core';
import { IModel } from '../Models/Base.model';
import { IResult, IResultList } from '../Models/Result.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ENVIRONMENT } from '../../../Environments/Environment';
import { Observable } from 'rxjs/Rx';
import { map } from 'rxjs/internal/operators';
import { DataStore } from '../../EntityModules/DataStore/data.store';
import { Pagination } from '../Models/Pagination.model';
import { DataModel } from '../../EntityModules/DataStore/DataModels/base.data.model';



export function Crud(
  path : string,
  Entity : Function,
  data : DataModel<any>,
  BASE_URL = ENVIRONMENT.BASE_SERVICE
) : Function {

  return function (target : any) {
    target.prototype.path = path;
    target.prototype.Entity = Entity;
    target.prototype.BASE_URL = BASE_URL;
    target.prototype.data = data;
    return target;
  };

}


/**
 * Абстрактный CRUD Сервис для работы с сервером
 * Его должны унаследовать все crud сервисы
 */
@Injectable()
export class CRUDService<T extends IModel> {

  protected BASE_URL : string;

  protected Entity : any;

  protected data : DataModel<T>;

  /**
   * Путь для круд операций
   */
  protected path : string;

  /**
   * Урл на который круд отправляет запросы
   */
  protected url : string = '';

  /**
   * Хедеры которые будут оправляться со всеми запросами
   */
  protected headers : HttpHeaders = new HttpHeaders();

  /**
   * Инициализация
   */
  constructor(
    protected httpClient : HttpClient,
    protected appData : DataStore
  ) {

    this.url = `${this.BASE_URL}/${this.path}`;
    this.headers.append('Accept', 'application/json');

  }

  /**
   * Метод для установки url
   * @param url - Строка вида 'user' по которому должен работать круд
   * @param API - Строка вида 'user' по которому должен работать круд
   */
  getUrl(url : any = '', API ? : string) : string {

    if(API) {
      return `${API}/${this.path}/${url}`;
    } else {
      return `${this.url}/${url}`;
    }
  }

  /**
   * GET запрос для получения списка моделей
   * @param params - Объект с данными для поиска
   * @returns {Observable<Object>}
   */
  query(params ? : HttpParams) : Observable<IResultList<T>> {

    return this.httpClient
      .get(this.getUrl(), {headers : this.headers, params}).pipe(
        map((res : IResultList<T>) : IResultList<T> => this.wrapObjects(res))
      );

  }

  /**
   * Получение модели по id
   * @param id - ID модели
   * @param params - Объект с данными для поиска
   * @returns {Observable<Object>}
   */
  get(id : number, params ? : HttpParams) : Observable<IResult<T>> {

    return this.httpClient
      .get(this.getUrl(id), {headers : this.headers, params}).pipe(
        map((res : IResult<T>) : IResult<T> => this.wrapObject(res))
      );

  }

  /**
   * Сохранение новой модели
   * @param data - Данные модели
   * @returns {Observable<Object>}
   */
  save(data : T) : Observable<IResult<T>> {

    return this.httpClient
      .post(this.getUrl(), data, {headers : this.headers}).pipe(
        map((res : IResult<T>) : IResult<T> => this.wrapObject(res))
      );
  }

  /**
   * Изменение существующей модели
   * @param data - Данные модели
   * @returns {Observable<Object>}
   */
  update(data : T) : Observable<IResult<T>> {

    return this.httpClient
      .put(this.getUrl(data.id), data, {headers : this.headers}).pipe(
        map((res : IResult<T>) : IResult<T> => this.wrapObject(res))
      );


  }

  /**
   * Удаление существующей модели
   * @param data - Данные модели
   * @returns {Observable<Object>}
   */
  remove(data : T) : Observable<IResult<T>> {

    return this.httpClient
      .delete(this.getUrl(data.id), {headers : this.headers}).pipe(
        map((res : IResult<T>) : IResult<T> => res)
      );

  }

  /**
   * Функция которую если переопредилить будет возвращать обернутые обьекты.
   * @param entity - Модель для генерации
   */
  createEntity(entity : T) : T {
    return new this.Entity(entity);
  }

  /**
   * Обарачивает данные с помощью функции createEntity для возврящяемыш данных IResultList
   * @param res - Ответ сервера
   * @returns {IResultList<T>}
   */
  protected wrapObjects(res : IResultList<T>) {
    res.data = res.data.map((entity : T) => {
      return this.createEntity(entity);
    });
    this.data.pagination.next(new Pagination(res));
    this.data.list.next(res.data);
    return res;
  }

  /**
   * Обарачивает данные с помощью функции createEntity для возврящяемыш данных IResult
   * @param res - Ответ сервера
   * @returns {IResult<T>}
   */
  protected wrapObject(res : IResult<T>) {
    res.data = this.createEntity(res.data);
    this.data.item.next(res.data);
    return res;
  }
}
