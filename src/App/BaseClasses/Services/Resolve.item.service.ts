import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { IResult } from '../Models/Result.model';
import { CRUDService } from './Crud.service';
import { IModel } from '../Models/Base.model';



export abstract class BaseItemResolver<T extends IModel> implements Resolve<T> {

  constructor(protected service : CRUDService<T>,
              protected router : Router) {
  }

  resolve(route : ActivatedRouteSnapshot, state : RouterStateSnapshot) : Observable<any> {

    let id : any = this.getParam(route);

    this.onStart();

    return this.query(id).pipe(
      map<any, any>(res => {
        return this.confirmData(res);
      }),
      catchError<any, any>((err : any, caught : Observable<any>) => {
        this.onError(err);
        throw caught;
      })
    );
  }

  protected getParam(route : ActivatedRouteSnapshot) {
    return route.paramMap.get('id')
  }

  protected query(id : number) {
    return this.service.get(id);
  }

  protected confirmData(res : IResult<any>) {
    if(res.data) {
      this.onSuccess();
      return res;
    }

    this.onError(res);
    return null;
  }

  protected onStart() {
  }

  protected onSuccess() {
    // console.log('success');
  }

  protected onError(error : any) {

    this.router.navigate(['/']);

    // switch (error.status) {
    //   case 500 :
    //     this.router.navigate(['/errors/500']);
    //     break;
    //   case 404 :
    //     this.router.navigate(['/errors/404']);
    //     break;
    //   case 403 :
    //     this.router.navigate(['/errors/403']);
    //     break;
    //   default :
    //     this.router.navigate(['/errors/500']);
    // }

    // document.getElementById('resolver-loader').style.display = 'none';
  }
}
