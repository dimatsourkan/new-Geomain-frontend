/**
 * Базовый класс компоненты
 */
import { Helpers, IHelpers } from '../Helpers';



export abstract class BaseComponent {

  helpers : IHelpers = Helpers;

}
