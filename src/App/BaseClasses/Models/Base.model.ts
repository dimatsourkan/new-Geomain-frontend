export interface IModel {
  id : number | string;
  created : string;
  modified : string;
  oldInstance : string;
  currentInstance : string;
  listenKeys : string[];

  isChanged : boolean;
  setOldModel();

}


export class BaseModel implements IModel {

  id : number | string;
  created : string;
  modified : string;
  oldInstance : string;
  listenKeys : string[] = [];

  constructor(model : any = {}) {

    if(!model) {
      model = {};
    }

    this.id = model.id;
    this.created = model.created;
    this.modified = model.modified;
  }

  setOldModel() {
    let old = {};

    this.listenKeys.forEach(k => {
      old[k] = this[k];
    });

    this.oldInstance = JSON.stringify(old);
  }

  get currentInstance() {

    let current = {};

    this.listenKeys.forEach(k => {
      current[k] = this[k];
    });

    return JSON.stringify(current);
  }

  get isChanged() {
    return this.oldInstance !== this.currentInstance;
  }
}
