import { HttpParams } from '@angular/common/http';

export interface IPagination {
  page : number;
  size : number;
  from : number;
  to : number;
  totalElements : number;
  totalPages : number;
  queryParams : HttpParams;
}

export class Pagination implements IPagination {

  page : number;
  size : number;
  totalElements : number;

  constructor(data : any = {}) {

    if(!data) {
      data = {};
    }

    this.page = data.page || 1;
    this.size = data.size || 20;
    this.totalElements = data.totalElements || 0;

  }

  get from() {
    return this.page * this.size - this.size + 1;
  }

  get to() {
    let to = this.page * this.size;
    if(to < this.totalElements) {
      return to;
    }
    else {
      return this.totalElements;
    }
  }

  get totalPages() {
    let total = (this.totalElements - (this.totalElements % this.size)) / this.size;
    if(total <= 0) {
      return 1;
    } else {
      return total;
    }
  }

  get queryParams() {
    let params = new HttpParams();
    params = params.set('size', this.size.toString());
    params = params.set('page', this.page.toString());
    return params;
  }
}