import { RouterModule, Routes } from '@angular/router';
import { WrapperComponent } from './Components/Wrapper/wrapper.component';
import { HomeComponent } from './AppModules/PageModules/Home/Home.component';
import { IsAuthenticated } from './BaseModules/Authorization/CanActivate/IsAuthenticated';
import { CanActivateApp } from './app.guard';
import { POPUPS_ROUTES } from './AppModules/Popups/Popups.routing';
import { USER_ROLE } from './EntityModules/User/User.model';
import { WrapperLightComponent } from './Components/WrapperLight/wrapper-light.component';



const routes : Routes = [

  ...POPUPS_ROUTES,

  {
    path : '',
    canActivate : [CanActivateApp],
    children : [
      {
        path : '',
        component : WrapperLightComponent,
        children : [
          {
            path : '',
            pathMatch: 'full',
            redirectTo : '/home',
          },
          {
            path : 'home',
            component : HomeComponent
          },
        ]
      },
      {
        path : '',
        component : WrapperComponent,
        children : [
          {
            path : 'about',
            loadChildren : './AppModules/PageModules/About/About.module#AboutModule',
          },
          {
            path : 'faq',
            loadChildren : './AppModules/PageModules/Faq/Faq.module#FaqModule',
          },
          {
            path : 'support',
            loadChildren : './AppModules/PageModules/Support/Support.module#SupportModule',
          },
          {
            path : 'privacy-policy',
            loadChildren : './AppModules/PageModules/PrivacyPolicy/PrivacyPolicy.module#PrivacyPolicyModule',
          },
          {
            path : 'terms',
            loadChildren : './AppModules/PageModules/Terms/Terms.module#TermsModule',
          },
          {
            path : 'my-geomains',
            loadChildren : './AppModules/PageModules/MyGeomains/MyGeomains.module#MyGeomainsModule',
          },
          {
            path : 'profile',
            canLoad : [IsAuthenticated],
            // data : { roles : [ USER_ROLE.ROLE_PERSONAL ] },
            loadChildren : './AppModules/PageModules/Profile/Profile.module#ProfileModule',
          },
          {
            path : 'gs/:number',
            loadChildren : './AppModules/PageModules/Geomain/Geomain.module#GeomainModule',
          },
          {
            path : 'gn/:number',
            loadChildren : './AppModules/PageModules/Geomain/Geomain.module#GeomainModule',
          },
          {
            path : 'business-cards',
            loadChildren : './AppModules/PageModules/BusinessCards/BusinessCards.module#BusinessCardsModule',
          },
          {
            path : 'registrar',
            loadChildren : './AppModules/PageModules/Registrar/Registrar.module#RegistrarModule',
          },
          {
            path : 'registrar/support',
            loadChildren : './AppModules/PageModules/Support/Support.module#SupportModule',
          },
        ]

      },

      {
        path : '*',
        redirectTo : '/home'
      }
    ]
  }
];

export const ROUTING = RouterModule.forRoot(routes);
