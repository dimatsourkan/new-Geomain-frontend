import { User } from '../../User/User.model';
import { BehaviorSubject } from 'rxjs/Rx';
import { DataModel } from './base.data.model';



/**
 * Модель для хранения данных пользователей
 */
export class UserDataModel extends DataModel<User> {

  public current = new BehaviorSubject<User>(new User());

  clearCurrent() {
    this.current.next(new User());
  }

  clearData() {
    this.clearCurrent();
    super.clearData();
  }

}