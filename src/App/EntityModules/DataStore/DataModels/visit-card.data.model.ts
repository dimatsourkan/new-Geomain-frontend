import { DataModel } from './base.data.model';
import { VisitCard } from '../../VisitCard/VisitCard.model';



/**
 * Модель для хранения данных пользователей
 */
export class VisitCardDataModel extends DataModel<VisitCard> {}