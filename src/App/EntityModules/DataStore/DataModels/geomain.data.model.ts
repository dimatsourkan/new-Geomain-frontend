import { BehaviorSubject } from 'rxjs/Rx';
import { DataModel } from './base.data.model';
import { Geomain } from '../../Geomain/Geomain.model';
import { GeomainImage } from '../../Geomain/Geomain-image.model';
import { GeomainMessage, GeomainMessageList, IGeomainMessage } from '../../Geomain/Geomain-message.model';



/**
 * Модель для хранения данных пользователей
 */
export class GeomainDataModel extends DataModel<Geomain> {

  public my = new BehaviorSubject<Geomain[]>(null);
  public images = new BehaviorSubject<GeomainImage[]>(null);
  public messages = new BehaviorSubject<GeomainMessage[]>(null);

  clearImages() {
    this.images.next(null);
  }

  clearData() {
    this.clearImages();
    super.clearData();
  }

}