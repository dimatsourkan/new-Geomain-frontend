import { BaseModel } from '../../../BaseClasses/Models/Base.model';
import { BehaviorSubject } from 'rxjs/Rx';
import { IPagination, Pagination } from '../../../BaseClasses/Models/Pagination.model';



/**
 * Общая модель для хранения данных
 */
export class DataModel<T extends BaseModel> {

  public pagination = new BehaviorSubject<Pagination>(new Pagination());
  public list = new BehaviorSubject<T[]>(null);
  public item = new BehaviorSubject<T>(null);

  clearList() {
    this.pagination.next(new Pagination());
    this.list.next([]);
  }

  clearItem() {
    this.item.next(null);
  }

  clearData() {
    this.clearList();
    this.clearItem();
  }
}