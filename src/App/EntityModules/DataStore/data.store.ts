import { Injectable } from '@angular/core';
import { UserDataModel } from './DataModels/user.data.model';
import { GeomainDataModel } from './DataModels/geomain.data.model';
import { VisitCardDataModel } from './DataModels/visit-card.data.model';


/**
 * Сервис для хранения данных
 */
@Injectable({
  providedIn : 'root'
})
export class DataStore {

  static visitCard = new VisitCardDataModel();
  static geomain = new GeomainDataModel();
  static geoname = new GeomainDataModel();
  static user    = new UserDataModel();

  constructor() {}

  isMine(id : number) {

    if(!DataStore.user.current.value) {
      return false;
    }

    if((!DataStore.user.current.value.id || !id)) {
      return false;
    }

    return id === DataStore.user.current.value.id;

  }

  static clearData() {
    DataStore.visitCard.clearData();
    DataStore.geomain.clearData();
    DataStore.geoname.clearData();
    DataStore.user.clearData();
  }
}