import { BaseModel, IModel } from '../../BaseClasses/Models/Base.model';
import { IUser, User } from '../User/User.model';
import { GeomainImage, IGeomainImage } from './Geomain-image.model';
import { GeomainMessageList, IGeomainMessage, IGeomainMessageList } from './Geomain-message.model';
import { Schedule } from './Geomain-schedule.model';



export const GEONUMBER_CATEGORY = {
  PERSONAL : 'PERSONAL',
  BUSINESS : 'BUSINESS',
  GOVERNMENT : 'GOVERNMENT'
};


export interface IGeomain extends IModel {
  about : string;
  category : string;
  expiryDate : string;
  email : string;
  phone : string;
  website : string;
  lat : number;
  lon : number;
  modified : string;
  number : number;
  name : string;
  numberMasked : string;
  owner : IUser;
  ownerId : number;
  premium : boolean;
  privacyProtect : boolean;
  qrCode : string;
  qrCodeLink : string;
  security : string;
  status : string;
  type : string;
  visitorPolicy : string;
  tag : string;
  previewImageURL : string;
  mainImage : string;
  openTime : string;
  linkedGeospot : IGeomain;

  schedule : Schedule;
}


export class Geomain extends BaseModel implements IGeomain {

  about : string;
  category : string;
  expiryDate : string;
  email : string;
  phone : string;
  website : string;
  lat : number;
  lon : number;
  modified : string;
  number : number;
  name : string;
  owner : IUser;
  ownerId : number;
  premium : boolean;
  privacyProtect : boolean;
  qrCode : string;
  qrCodeLink : string;
  security : string;
  status : string;
  type : string;
  visitorPolicy : string;
  tag : string;
  previewImageURL : string;
  openTime : string;
  linkedGeospot : Geomain;

  listenKeys = [
    'about',
    'email',
    'phone',
    'website'
  ];

  constructor(model : any = {}) {

    super(model);

    if(!model) {
      model = {};
    }

    this.about = model.about || null;
    this.category = model.category || null;
    this.expiryDate = model.expiryDate || null;
    this.email = model.email || null;
    this.phone = model.phone || null;
    this.website = model.website || null;
    this.lat = model.lat || null;
    this.lon = model.lon || null;
    this.modified = model.modified || null;
    this.number = model.number || null;
    this.name = model.name || null;
    this.owner = new User(model.owner);
    this.ownerId = model.ownerId || null;
    this.premium = model.premium || false;
    this.privacyProtect = model.privacyProtect || false;
    this.qrCode = model.qrCode || null;
    this.qrCodeLink = model.qrCodeLink || null;
    this.security = model.security || null;
    this.status = model.status || null;
    this.type = model.type || null;
    this.visitorPolicy = model.visitorPolicy || null;
    this.tag = model.tag || null;
    this.previewImageURL = model.previewImageURL || null;
    this.openTime = model.openTime || null;
    this.linkedGeospot = model.linkedGeospot ? new Geomain(model.linkedGeospot) : null;

    this.setOldModel();
  }



  get mainImage() : string {
    return this.previewImageURL;
  }

  get numberMasked() : string {
    let masked : string = '';

    if(this.number) {
      let num : string = this.number.toString();
      for (let i = 1; i <= num.length; i++) {
        masked += num[i - 1];
        if(i % 4 == 0) {
          masked += '-';
        }
      }
    }

    return masked;
  }

  get schedule() {
    let data = {};
    try {
      data = JSON.parse(this.openTime)
    }
    catch(err) {

    }
    return new Schedule(data);
  }
}
