import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { IResult } from '../../BaseClasses/Models/Result.model';
import { BaseItemResolver } from '../../BaseClasses/Services/Resolve.item.service';
import { IGeomain } from './Geomain.model';
import { GeomainService } from './Geomain.service';



@Injectable({
    providedIn : 'root'
  })
export class GeomainResolver extends BaseItemResolver<IGeomain> implements Resolve<IResult<IGeomain>> {

  constructor(protected service : GeomainService,
              protected router : Router) {
    super(service, router);
  }

  protected getParam(route : ActivatedRouteSnapshot) {
    return route.paramMap.get('number')
  }

  protected query(number : number) {
    return this.service.find(number);
  }
}
