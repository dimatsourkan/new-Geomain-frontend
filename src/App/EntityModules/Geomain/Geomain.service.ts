import { Injectable } from '@angular/core';
import { Crud, CRUDService } from '../../BaseClasses/Services/Crud.service';
import { Geomain, IGeomain } from './Geomain.model';
import { map } from 'rxjs/internal/operators';
import { IResult, IResultList } from '../../BaseClasses/Models/Result.model';
import { GeomainImage, IGeomainImage } from './Geomain-image.model';
import { Observable } from 'rxjs/Rx';
import { DataStore } from '../DataStore/data.store';
import { GeomainDataModel } from '../DataStore/DataModels/geomain.data.model';
import { GeomainMessage, GeomainMessageList, IGeomainMessage } from './Geomain-message.model';


const GEOMAIN_API = 'http://34.240.206.145:8081/api';

/**
 * Сервис для работы с пользователями
 */
@Injectable({
  providedIn : 'root'
})
@Crud('geomain', Geomain, DataStore.geomain)
export class GeomainService extends CRUDService<IGeomain> {

  data : GeomainDataModel;

  private _code : string = '';

  set Code(code : string) {
    this._code = code;
  }

  removeGeospot(name : any) : Observable<any> {

    return this.httpClient
      .delete(this.getUrl(), {headers : this.headers, params : { name }});

  }

  /**
   * Методы для работы с геомайнами
   * @returns {Observable<IGeomain[]>}
   */
  list() {
    return this.httpClient
      .get(this.getUrl('geomains'), {headers : this.headers}).pipe(
        map((res : IResultList<IGeomain>) : IResultList<IGeomain> => this.wrapObjects(res))
      );
  }

  find(geomain : any) {
    return this.httpClient
      .get(this.getUrl(`find/${geomain}`),
        {headers : this.headers, params : {value : this._code}}
      ).pipe(
        map((res : IResult<IGeomain>) : IResult<IGeomain> => {
          res.data = res.data[0];
          return this.wrapObject(res);
        })
      );
  }

  search(geomain : string) {
    return this.httpClient
      .get(this.getUrl('search'), {headers : this.headers, params : {geomain}}).pipe(
        map<IGeomain, IGeomain>((res) => this.createEntity(res))
      );
  }

  updatePolicy(geomain : any, visitorPolicy : string) {
    return this.httpClient
      .put(this.getUrl('visitor-policy'), {}, {
        headers: this.headers,
        params : { geomain, visitorPolicy }
      });
  }

  updateGeomainSeq(data : { security : string, geomain ?: number, value ?: string }) {
    return this.httpClient
      .put( this.getUrl('security'), data, {headers: this.headers}).pipe(
        map<any, any>((res) => {
          this.data.item.subscribe(item => {
            item.security = data.security;
          });
          return res;
        })
      );
  }

  updateGeomain(geomain : IGeomain) {
    return this.httpClient
      .put(this.getUrl(geomain.number), geomain, {headers: this.headers});
  }

  /**
   * Методы для работы с feed
   * @param {number} geomain
   * @returns {Observable<Object>}
   */
  getMessages(geomain : any) {
    return this.httpClient.get(
      this.getUrl(`feed`),
      {headers : this.headers, params : { geomain }}
      ).pipe(
        map((res : any) => {
          this.data.messages.next(GeomainMessage.wrapMessages(res.data));
        })
    )
  }

  postMessage(geomain : number, message : string) {
    return this.httpClient.post(
      this.getUrl(`feed`),
      {geomain, message},
      {headers : this.headers}
      )
  }

  deleteMessage(id : number) {
    return this.httpClient.delete(
      this.getUrl(`feed`), {headers : this.headers, params : { id : `${id}` }}).pipe(
      map<any, any>((res) => {
        this.data.messages.next(this.data.messages.value.filter(m => m.id !== id));
        return res;
      })
    );
  }

  /**
   * Методы для работы с изображениями геомайнов
   * @param {number | string} geomain
   * @param form
   * @returns {Observable<IGeomain>}
   */
  /**
   * Методы для работы с геомайнами
   * @returns {Observable<IGeomain[]>}
   */
  getImages(geomain : number|string) {
    return this.httpClient
      .get(this.getUrl(`image/${geomain}`), {headers : this.headers}).pipe(
        map<IGeomainImage[], IGeomainImage[]>((res) => {

          let images = GeomainImage.wrapImages(res);
          this.data.images.next(images);

          return images;
        })
      );
  }

  uploadImage(geomain : number|string, form : any) {
    return this.httpClient
      .post(this.getUrl(`image/${geomain}`, GEOMAIN_API), new FormData(form), {headers : this.headers}).pipe(
        map<IGeomainImage[], IGeomainImage[]>((res) => {

          let images = GeomainImage.wrapImages(res);
          this.data.images.next(images);
          return images;
        })
      );
  }

  deleteImage(id : number) {
    return this.httpClient.delete(this.getUrl(`image/${id}`), {headers : this.headers}).pipe(
      map<IGeomainImage[], IGeomainImage[]>((res) => {

        let images = GeomainImage.wrapImages(res);
        this.data.images.next(images);
        return images;
      })
    );
  }

  /**
   * Методы для работы с рекламой геомайна
   * @param {number | string} geomain
   * @param form
   * @returns {Observable<IGeomain>}
   */
  uploadAdVideo(geomain : number|string, form : any) {
    return this.httpClient
      .post(this.getUrl(`ad/vemb`, GEOMAIN_API), new FormData(form), {headers : this.headers});
  }

  getAdvertising(id : number|string) {
    return this.httpClient
      .get(this.getUrl(`ad/${id}`), {headers : this.headers});
  }
}
