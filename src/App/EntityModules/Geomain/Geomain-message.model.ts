import { BaseModel, IModel } from '../../BaseClasses/Models/Base.model';



export interface IGeomainMessage extends IModel {
  message : string;
}


export class GeomainMessage extends BaseModel implements IGeomainMessage {

  message : string;

  static wrapMessages(data : any[] = []) {

    if(!data) {
      data = [];
    }

    return data.map((d) => new GeomainMessage(d));
  }

  constructor(model : any = {}) {

    super(model);

    if(!model) {
      model = {};
    }

    this.message = model.message || null;

  }
}

export interface IGeomainMessageList {
  data : IGeomainMessage[];
  page : number;
  totalElements : number;
}

export class GeomainMessageList {
  data : IGeomainMessage[];
  page : number;
  totalElements : number;

  constructor(model : any = {}) {
    if(!model) {
      model = {};
    }

    this.page = model.page || 0;
    this.totalElements = model.totalElements || 0;
    this.data = GeomainMessage.wrapMessages(model.data);
  }
}
