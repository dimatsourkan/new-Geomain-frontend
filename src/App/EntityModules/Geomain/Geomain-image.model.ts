import { BaseModel, IModel } from '../../BaseClasses/Models/Base.model';



export interface IGeomainImage extends IModel {
  imageUrl : string;
  positionIndex : number;
}


export class GeomainImage extends BaseModel implements IGeomainImage {

  imageUrl : string;
  positionIndex : number;

  static wrapImages(data : any[] = []) {
    return data.map((d) => new GeomainImage(d));
  }

  constructor(model : any = {}) {

    super(model);

    if(!model) {
      model = {};
    }

    this.imageUrl = model.imageUrl || null;
    this.positionIndex = model.positionIndex || 0;

  }
}
