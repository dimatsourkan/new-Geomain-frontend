class ScheduleDay {
  timeFrom : string;
  timeTo : string;
  enabled : boolean;

  constructor(day : any = {}) {

    if(!day) {
      day = {};
    }

    this.timeFrom = day.timeFrom || '10:00';
    this.timeTo = day.timeTo || '18:00';
    this.enabled = day.enabled || false;

  }
}


export class Schedule {

  MO : ScheduleDay;
  TU : ScheduleDay;
  WE : ScheduleDay;
  TH : ScheduleDay;
  FR : ScheduleDay;
  SA : ScheduleDay;
  SU : ScheduleDay;
  today : ScheduleDay;

  get daysList() : ScheduleDay[] {
    return [this.MO, this.TU, this.WE, this.TH, this.FR, this.SA, this.SU];
  }

  constructor(schedule : any = {}) {

    if(!schedule) {
      schedule = {};
    }

    this.MO = new ScheduleDay(schedule.MO);
    this.TU = new ScheduleDay(schedule.TU);
    this.WE = new ScheduleDay(schedule.WE);
    this.TH = new ScheduleDay(schedule.TH);
    this.FR = new ScheduleDay(schedule.FR);
    this.SA = new ScheduleDay(schedule.SA);
    this.SU = new ScheduleDay(schedule.SU);

    this.today = this.daysList[(new Date()).getDay()-1];
  }

}
