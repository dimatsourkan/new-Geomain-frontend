import { Injectable } from '@angular/core';
import { Crud, CRUDService } from '../../BaseClasses/Services/Crud.service';
import { Geomain, IGeomain } from './Geomain.model';
import { map } from 'rxjs/internal/operators';
import { IQueryData } from '../../BaseClasses/Models/Result.model';
import { DataStore } from '../DataStore/data.store';
import { GeomainDataModel } from '../DataStore/DataModels/geomain.data.model';
import { ENVIRONMENT } from '../../../Environments/Environment';

/**
 * Сервис для работы с пользователями
 */
@Injectable({
  providedIn : 'root'
})
@Crud('geoname', Geomain, DataStore.geoname)
export class GeonameService extends CRUDService<Geomain> {

  data : GeomainDataModel;

  list() {
    return this.httpClient
      .get(`${ENVIRONMENT.BASE_SERVICE}/geomain/geonames`, {headers : this.headers}).pipe(
        map((res : IQueryData<Geomain>) : IQueryData<Geomain> => {
          res.data = res.data.map((entity : Geomain) => {
            return this.createEntity(entity);
          });
          this.data.list.next(res.data);
          return res;
        })
      );
  }


  linkGnamesToGnumberById(geonameIds : any[], geonumberId : any) {
    return this.httpClient
      .post(this.getUrl('assign-gNames-to-gNumber-by-id'),
        {geonameIds, geonumberId},
        {headers: this.headers});
  }
}
