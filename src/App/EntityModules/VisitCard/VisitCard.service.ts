import { Injectable } from '@angular/core';
import { Crud, CRUDService } from '../../BaseClasses/Services/Crud.service';
import { IVisitCard, VisitCard } from './VisitCard.model';
import { ENVIRONMENT } from '../../../Environments/Environment';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IResult, IResultList } from '../../BaseClasses/Models/Result.model';
import { map } from 'rxjs/operators';
import { DataStore } from '../DataStore/data.store';
import { VisitCardDataModel } from '../DataStore/DataModels/visit-card.data.model';


/**
 * Сервис для работы с визитными картами
 */
@Injectable({
  providedIn : 'root'
})
@Crud('user/visit', VisitCard, DataStore.visitCard, ENVIRONMENT.USER_SERVICE)
export class VisitCardService extends CRUDService<IVisitCard> {

  data : VisitCardDataModel;

  all(params ? : HttpParams) : Observable<IResultList<IVisitCard>> {
    return this.httpClient
      .get(this.getUrl('all'), {headers : this.headers, params}).pipe(
        map((res : IResultList<IVisitCard>) : IResultList<IVisitCard> => this.wrapObjects(res))
      );

  }

  my(params ? : HttpParams) : Observable<IResultList<IVisitCard>> {
    return this.httpClient
      .get(this.getUrl('my'), {headers : this.headers, params}).pipe(
        map((res : IResultList<IVisitCard>) : IResultList<IVisitCard> => this.wrapObjects(res))
      );

  }

  collected(params ? : HttpParams) : Observable<IResultList<IVisitCard>> {
    return this.httpClient
      .get(this.getUrl('collected'), {headers : this.headers, params}).pipe(
        map((res : IResultList<IVisitCard>) : IResultList<IVisitCard> => this.wrapObjects(res))
      );

  }

  protected wrapObject(res : any) {
    res = this.createEntity(res);
    this.data.item.next(res);
    return res;
  }

}
