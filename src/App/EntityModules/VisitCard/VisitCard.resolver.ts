import { Injectable } from '@angular/core';
import { Router, Resolve } from '@angular/router';
import { IResult } from '../../BaseClasses/Models/Result.model';
import { BaseItemResolver } from '../../BaseClasses/Services/Resolve.item.service';
import { IVisitCard } from './VisitCard.model';
import { VisitCardService } from './VisitCard.service';



@Injectable({
    providedIn : 'root'
  })
export class VisitCardResolver extends BaseItemResolver<IVisitCard> implements Resolve<IResult<IVisitCard>> {

  constructor(protected service : VisitCardService,
              protected router : Router) {
    super(service, router);
  }

  protected confirmData(res : IResult<any>) {
    if(res) {
      this.onSuccess();
      return res;
    }

    this.onError(res);
    return null;
  }

}
