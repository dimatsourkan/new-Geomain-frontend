import { BaseModel, IModel } from '../../BaseClasses/Models/Base.model';
import { IUser, User } from '../User/User.model';

export interface IVisitCard extends IModel {

  bcardName : string;
  cardName : string;
  email : string;
  firstName : string;
  gnameId : number;
  lastName : string;
  organization : string;
  owner : IUser;
  phone : string;
  position : string;
  qrCodeLink : string;
  visitImage : string;
  website : string;
  workPhone : string;

}

export class VisitCard extends BaseModel implements IVisitCard {

  bcardName : string;
  cardName : string;
  email : string;
  firstName : string;
  gnameId : number;
  lastName : string;
  organization : string;
  owner : IUser;
  phone : string;
  position : string;
  qrCodeLink : string;
  visitImage : string;
  website : string;
  workPhone : string;

  constructor(model ? : any) {
    super(model);

    if(!model) {
      model = {};
    }

    this.bcardName = model.bcardName || null;
    this.cardName = model.cardName || null;
    this.email = model.email || null;
    this.firstName = model.firstName || null;
    this.gnameId = model.gnameId || null;
    this.lastName = model.lastName || null;
    this.organization = model.organization || null;
    this.owner = new User(model.owner);
    this.phone = model.phone || null;
    this.position = model.position || null;
    this.qrCodeLink = model.qrCodeLink || null;
    this.visitImage = model.visitImage || null;
    this.website = model.website || null;
    this.workPhone = model.workPhone || null;

  }
}