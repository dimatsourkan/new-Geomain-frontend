import { Injectable } from '@angular/core';
import { Crud, CRUDService } from '../../BaseClasses/Services/Crud.service';
import { IUser, User } from './User.model';
import { Observable } from 'rxjs/Rx';
import { map } from 'rxjs/internal/operators';
import { DataStore } from '../DataStore/data.store';
import { IQueryData } from '../../BaseClasses/Models/Result.model';
import { Geomain, IGeomain } from '../Geomain/Geomain.model';
import { ENVIRONMENT } from '../../../Environments/Environment';
/**
 * Сервис для работы с пользователями
 */
@Injectable({
  providedIn : 'root'
})
@Crud('registrar', User, DataStore.user)
export class RegistrarService extends CRUDService<IUser> {

  infoUrl(path : string) : string {
    return `${ENVIRONMENT.BASE_SERVICE}/registrar-info/${path}`;
  }

  dashboardSummary(period : any) : Observable<any> {
    return this.httpClient.get(this.getUrl('dashboard-summary'),
      {headers: this.headers, params : { period }}
      )
      .pipe(
        map<any, any>((res) => {
          return {
            amountCreditedGeomains : res.amountCreditedGeomains || 0,
            amountDebitedAddons : res.amountDebitedAddons || 0,
            countNumberGeomain : res.countNumberGeomain || 0,
            countRegistrants : res.countRegistrants || 0
          }
        })
      );
  }

  getMyGeonames() {
    return this.httpClient
      .get(this.infoUrl('geonames'), {headers : this.headers}).pipe(
        map((res : IQueryData<Geomain>) : IQueryData<Geomain> => {
          res.data = res.data.map((entity : Geomain) => {
            return new Geomain(entity);
          });
          DataStore.geoname.my.next(res.data);
          return res;
        })
      );
  }

}
