import { Injectable } from '@angular/core';
import { Crud, CRUDService } from '../../BaseClasses/Services/Crud.service';
import { IUser, User } from './User.model';
import { IResult } from '../../BaseClasses/Models/Result.model';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/internal/operators';
import { ENVIRONMENT } from '../../../Environments/Environment';
import { ISupportData } from '../../AppModules/PageModules/Support/Support.component';
import { DataStore } from '../DataStore/data.store';
import { UserDataModel } from '../DataStore/DataModels/user.data.model';


const USER_API = 'http://34.240.206.145:8080/api';
/**
 * Сервис для работы с пользователями
 */
@Injectable({
  providedIn : 'root'
})
@Crud('user', User, DataStore.user, ENVIRONMENT.USER_SERVICE)
export class UserService extends CRUDService<IUser> {

  data : UserDataModel;

  /**
   * Получение данных профиля
   * @returns {Observable<IResult<IUser>>}
   */
  getProfile() : Observable<IUser> {
    return this.httpClient
      .get(this.getUrl('profile'), {headers : this.headers}).pipe(
        map<IUser, IUser>((res) => this.createEntity(res)),
        map<IUser, IUser>((res) => {
          this.data.current.next(res);
          return res;
        })
      );
  }

  uploadAvatar(form : any) {
    return this.httpClient
      .post(this.getUrl('photo', USER_API), new FormData(form), {headers : this.headers}).pipe(
        map<IUser, IUser>((res) => this.createEntity(res))
      );
  }

  deleteAvatar() : Observable<any> {
    return this.httpClient
      .delete(this.getUrl('photo'), {headers : this.headers});
  }

  support(data : ISupportData) {
    return this.httpClient.post( `${ENVIRONMENT.BASE_SERVICE}/open/support`, data, {
      headers : this.headers
    });
  }

}
