import {VisitCardComponent} from "./VisitCard.component";
import {NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
      CommonModule
    ],
    declarations: [
        VisitCardComponent
    ],
    exports : [
        VisitCardComponent
    ],
    providers: []
})

export class VisitCardModule {}
