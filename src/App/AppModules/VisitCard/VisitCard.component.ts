import { Component, Input } from '@angular/core';
import { IVisitCard } from '../../EntityModules/VisitCard/VisitCard.model';



@Component({
  selector : 'visit-card',
  templateUrl : './VisitCard.component.html',
  styleUrls : ['./VisitCard.component.less']
})

export class VisitCardComponent {
  @Input() card : IVisitCard;
}
