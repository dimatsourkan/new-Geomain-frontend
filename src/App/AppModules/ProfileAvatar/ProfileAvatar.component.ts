import { Component, ElementRef, ViewChild } from '@angular/core';
import { ConfirmModal } from '../../BaseModules/Modal/Components/Confirm/Confirm.component';
import { Loader } from '../../BaseModules/Loader/Loader.component';
import { IUser } from '../../EntityModules/User/User.model';
import { DataStore } from '../../EntityModules/DataStore/data.store';
import { UserService } from '../../EntityModules/User/Users.service';
import { BaseComponent } from '../../BaseClasses/Components/Base.component';



@Component({
  selector : 'profile-avatar',
  templateUrl : './ProfileAvatar.component.html',
  styleUrls : ['./ProfileAvatar.component.less']
})

export class ProfileAvatarComponent extends BaseComponent {

  @ViewChild('confirm') private confirm : ConfirmModal;
  @ViewChild('loader') private loader : Loader;

  user : IUser;

  constructor(private userService : UserService) {
    super();
    this.user = DataStore.user.current.value;
    DataStore.user.current.subscribe(user => this.user = user);
  }

  removeAvatar() {
    this.confirm.open(() => {
      this.loader.show();
      this.confirm.close();
      this.userService.deleteAvatar().subscribe(res => {
        this.userService.getProfile().subscribe();
        this.loader.hide();
      }, (err : any) => {
        this.loader.hide();
      });
    });
  }

  uploadAvatar(fileForm : ElementRef) {
    this.loader.show();
    this.userService.uploadAvatar(fileForm).subscribe(() => {
      this.userService.getProfile().subscribe();
      this.loader.hide();
    }, err => {
      this.loader.hide();
    });
  }

}
