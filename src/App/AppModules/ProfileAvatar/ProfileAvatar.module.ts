import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderModule } from '../../BaseModules/Loader/Loader.module';
import { ProfileAvatarComponent } from './ProfileAvatar.component';
import { ModalModule } from '../../BaseModules/Modal/Modal.module';



@NgModule({
  imports : [
    CommonModule,
    LoaderModule,
    ModalModule,
  ],
  declarations : [
    ProfileAvatarComponent
  ],
  exports : [
    ProfileAvatarComponent
  ],
  providers : []
})

export class ProfileAvatarModule {
}
