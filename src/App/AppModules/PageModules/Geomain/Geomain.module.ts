import { GeomainComponent } from './Geomain.component';
import { NgModule } from '@angular/core';
import { GEOSPOT_ROUTING } from './Geomain.routing';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { GeospotInfoComponent } from './Components/Info/Info.component';
import { FormControlsModule } from '../../../BaseModules/FormControls/FormControls.module';
import { ENVIRONMENT } from '../../../../Environments/Environment';
import { AgmCoreModule } from '@agm/core';
import { GeospotAdvertisingComponent } from './Components/Advertising/Advertising.component';
import { GeomainComponentsModule } from '../../Geomain/Geomain.module';
import { GeospotPrivacyComponent } from './Components/Privacy/Privacy.component';
import { GeospotActivityComponent } from './Components/Activity/Activity.component';
import { ModalModule } from '../../../BaseModules/Modal/Modal.module';
import { FormsModule } from '@angular/forms';
import { LoaderModule } from '../../../BaseModules/Loader/Loader.module';
import { SaveChangesModule } from '../../../BaseModules/SaveChanges/SaveChanges.module';
import { FixOnScrollModule } from '../../../BaseModules/FixOnScroll/FixOnScroll.module';



@NgModule({
  imports : [
    GEOSPOT_ROUTING,
    FormsModule,
    RouterModule,
    CommonModule,
    ModalModule,
    LoaderModule,
    FixOnScrollModule,
    SaveChangesModule,
    FormControlsModule,
    GeomainComponentsModule,
    AgmCoreModule.forRoot({
      apiKey : ENVIRONMENT.MAP_API_KEY
    })
  ],
  declarations : [
    GeomainComponent,
    GeospotInfoComponent,
    GeospotPrivacyComponent,
    GeospotActivityComponent,
    GeospotAdvertisingComponent
  ],
  exports : [],
  providers : []
})

export class GeomainModule {
}
