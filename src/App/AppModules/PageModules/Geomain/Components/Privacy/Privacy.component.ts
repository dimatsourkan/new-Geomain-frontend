import { Component, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../../../BaseClasses/Components/Base.component';
import { GeomainDeleteComponent } from '../../../../Geomain/Components/Delete/Delete.component';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { GeomainService } from '../../../../../EntityModules/Geomain/Geomain.service';
import { Loader } from '../../../../../BaseModules/Loader/Loader.component';
import { Router } from '@angular/router';
import { Geomain, IGeomain } from '../../../../../EntityModules/Geomain/Geomain.model';



@Component({
  selector : 'geospot-privacy',
  templateUrl : './Privacy.component.html',
  styleUrls : ['./Privacy.component.less']
})

export class GeospotPrivacyComponent extends BaseComponent {

  @ViewChild('confirmDelete') private confirmDelete : GeomainDeleteComponent;
  @ViewChild('codeLoader') private codeLoader : Loader;
  @ViewChild('loader') private loader : Loader;

  code : string = '';

  item = new Geomain();

  constructor(private geomainService : GeomainService,
              private router : Router
  ) {
    super();
    let item = DataStore.geomain.item.value;
    this.item  = item.linkedGeospot || item;
  }

  deleteGeospot() {
    this.confirmDelete.open(() => {
      this.loader.show();
      this.geomainService.removeGeospot(this.item.number).subscribe(() => {
        this.loader.hide();
        this.confirmDelete.close();
        this.router.navigate(['/my-geomains', 'geospots']);
      }, err => {
        this.loader.hide();
      });
    });
  }

  toggleSequrity(checked : boolean) {
    this.code = '';
    if(!checked) {
      this.disableSecutity();
    }
  }

  savePinSequrity() {
    this.updateSequrity({
      security : 'PIN',
      value : this.code
    });
  }

  disableSecutity() {
    this.updateSequrity({
      security : 'DISABLED'
    });
  }

  updateSequrity(data : { security : string, value ? : string, geomain ? : number }) {
    this.codeLoader.show();
    data.geomain = this.item.number;
    this.geomainService.updateGeomainSeq(data).subscribe(res => {
      this.codeLoader.hide();
      this.code = '';
    }, err => {
      this.codeLoader.hide();
    });
  }

}
