import { Component, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../../../BaseClasses/Components/Base.component';
import { VISITOR_POLICY } from '../../../../../EntityModules/Geomain/visitorPolicy';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { GeomainService } from '../../../../../EntityModules/Geomain/Geomain.service';
import { Loader } from '../../../../../BaseModules/Loader/Loader.component';
import { Schedule } from '../../../../../EntityModules/Geomain/Geomain-schedule.model';
import { Geomain, IGeomain } from '../../../../../EntityModules/Geomain/Geomain.model';
import { Observable } from 'rxjs/Rx';
import { map } from 'rxjs/internal/operators';
import { GeonameService } from '../../../../../EntityModules/Geomain/Geoname.service';



@Component({
  selector : 'geospot-info',
  templateUrl : './Info.component.html',
  styleUrls : ['./Info.component.less']
})

export class GeospotInfoComponent extends BaseComponent {

  @ViewChild('loader') private loader : Loader;
  @ViewChild('infoLoader') private infoLoader : Loader;
  @ViewChild('gnamesLoader') private gnamesLoader : Loader;
  @ViewChild('workingHoursLoader') private workingHoursLoader : Loader;

  VISITOR_POLICY = VISITOR_POLICY;

  linkedGeonames : IGeomain[] = [];

  item = new Geomain();
  itemName = new Geomain();

  list$ : Observable<IGeomain[]>;

  constructor(
    private geomainService : GeomainService,
    private geonameService : GeonameService
  ) {
    super();
    let linked = DataStore.geomain.item.value.linkedGeospot;
    this.item  = linked || DataStore.geomain.item.value;
    this.list$ = DataStore.geoname.list;
    this.itemName = DataStore.geomain.item.value;

    this.getGeonames();
  }

  getGeonames() {
    this.geonameService.list().subscribe(() => {
      this.getFirstGeonameLink().subscribe(list => {
        this.linkedGeonames = list;
      });
    });
  }

  getFirstGeonameLink() {

    return this.list$.pipe(map(list => {

      if(!list.length) {
        return null;
      }

      return list.filter((g : IGeomain) => {

        if(g.linkedGeospot) {
          return g.linkedGeospot.id == this.item.id;
        }
        else {
          return false;
        }

      });

    }));

  }

  linkGnames(id : number) {

    this.gnamesLoader.show();

    let gnameIds = this.linkedGeonames.map(g => g.id);
    let index    = gnameIds.indexOf(id);

    if(index >= 0) {
      gnameIds.splice(index, 1);
    } else {
      gnameIds.push(id);
    }

    this.geonameService.linkGnamesToGnumberById(gnameIds, this.item.id).subscribe(
      () => {
        this.getGeonames();
        this.gnamesLoader.hide();
      },
      () => this.gnamesLoader.hide()
    )
  }

  updatePolicy() {
    this.infoLoader.show();
    this.geomainService.updatePolicy(this.item.number, this.item.visitorPolicy)
      .subscribe(
        () => this.infoLoader.hide(),
        () => this.infoLoader.hide()
      );
  }

  updateWorkingHours(schedule : Schedule) {

    this.workingHoursLoader.show();
    this.item.openTime = JSON.stringify(schedule);

    this.geomainService.updateGeomain(this.item).subscribe(
      () => this.workingHoursLoader.hide(),
      () => this.workingHoursLoader.hide()
    );

  }

  updateGeomain() {
    this.loader.show();
    this.geomainService.updateGeomain(this.item).subscribe(
      () => {
        this.item.oldInstance = this.item.currentInstance;
        this.loader.hide();
      },
      () => this.loader.hide()
    )
  }

}
