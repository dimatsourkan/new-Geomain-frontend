import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../../../BaseClasses/Components/Base.component';
import { GeomainService } from '../../../../../EntityModules/Geomain/Geomain.service';
import { ConfirmModal } from '../../../../../BaseModules/Modal/Components/Confirm/Confirm.component';
import { Loader } from '../../../../../BaseModules/Loader/Loader.component';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { Geomain } from '../../../../../EntityModules/Geomain/Geomain.model';
import { finalize } from 'rxjs/operators';



@Component({
  selector : 'geospot-activity',
  templateUrl : './Activity.component.html',
  styleUrls : ['./Activity.component.less']
})

export class GeospotActivityComponent extends BaseComponent implements OnInit {

  @ViewChild('loader') private loader : Loader;
  @ViewChild('confirm') private confirm : ConfirmModal;
  @ViewChild('deleteLoader') private deleteLoader : Loader;

  item = new Geomain();
  message : string = '';
  messages$ = DataStore.geoname.messages;

  constructor(
    public geomainService : GeomainService
  ) {
    super();
    const item : Geomain = DataStore.geomain.item.value;
    this.item = item.linkedGeospot || item;
  }

  ngOnInit() {
    this.geomainService.getMessages(this.item.number).subscribe();
  }

  getGeomain() {
    this.geomainService.find(this.item.number).subscribe()
  }

  postMessage() {
    this.loader.show();
    this.geomainService.postMessage(this.item.number, this.message).subscribe(() => {
      this.getGeomain();
      this.message = '';
      this.loader.hide();
    });
  }

  deleteMessage(id : number) {
    this.confirm.open(() => {
      this.deleteLoader.show();
      this.geomainService.deleteMessage(id).subscribe(() => {
        this.confirm.close();
        this.deleteLoader.hide();
      }, () => this.deleteLoader.hide());
    });
  }

}
