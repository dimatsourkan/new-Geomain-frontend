import { Component } from '@angular/core';
import { BaseComponent } from '../../../../../BaseClasses/Components/Base.component';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { Geomain } from '../../../../../EntityModules/Geomain/Geomain.model';



@Component({
  selector : 'geospot-advertising',
  templateUrl : './Advertising.component.html',
  styleUrls : ['./Advertising.component.less']
})

export class GeospotAdvertisingComponent extends BaseComponent {

  item = new Geomain();

  constructor() {
    super();
    let item : Geomain = DataStore.geomain.item.value;
    this.item  = item.linkedGeospot || item;
  }

}
