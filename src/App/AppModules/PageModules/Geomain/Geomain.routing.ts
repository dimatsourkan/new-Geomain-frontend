import { RouterModule, Routes } from '@angular/router';
import { GeomainComponent } from './Geomain.component';
import { GeomainResolver } from '../../../EntityModules/Geomain/Geomain.resolver';
import { GeospotInfoComponent } from './Components/Info/Info.component';
import { GeospotAdvertisingComponent } from './Components/Advertising/Advertising.component';
import { GeospotPrivacyComponent } from './Components/Privacy/Privacy.component';
import { GeospotActivityComponent } from './Components/Activity/Activity.component';



export const routes : Routes = [
  {
    path : '',
    component : GeomainComponent,
    resolve : {
      geomain : GeomainResolver
    },
    children : [
      {
        path : '',
        pathMatch: 'full',
        redirectTo : 'info',
      },
      {
        path : 'info',
        component : GeospotInfoComponent
      },
      // {
      //   path : 'advertising',
      //   component : GeospotAdvertisingComponent
      // },
      {
        path : 'privacy',
        component : GeospotPrivacyComponent
      },
      {
        path : 'activity',
        component : GeospotActivityComponent
      }
    ]
  }
];

export const GEOSPOT_ROUTING = RouterModule.forChild(routes);