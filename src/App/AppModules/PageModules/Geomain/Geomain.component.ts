import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BaseComponent } from '../../../BaseClasses/Components/Base.component';
import { ShareComponent } from '../../Geomain/Components/Share/Share.component';
import { DataStore } from '../../../EntityModules/DataStore/data.store';
import { Observable } from 'rxjs/Rx';
import { IGeomain } from '../../../EntityModules/Geomain/Geomain.model';



@Component({
  selector : 'geospot-page',
  templateUrl : './Geomain.component.html',
  styleUrls : ['./Geomain.component.less']
})

export class GeomainComponent extends BaseComponent implements OnInit {

  @ViewChild('share') private share : ShareComponent;

  item : IGeomain;
  itemName : IGeomain;

  constructor(
    private router : Router
  ) {
    super();
    DataStore.geomain.item.subscribe(item => {
      this.item = item.linkedGeospot || item;
      this.itemName = item;
    });
  }

  openShare() {
    this.share.open();
  }

  ngOnInit() {

    /**
     * Отслеживание изменения роутинга, для скролла по якорям
     */
    this.router.events.subscribe(s => {

      if (s instanceof NavigationEnd) {

        const tree = this.router.parseUrl(this.router.url);

        if (tree.fragment) {
          const element : any = document.querySelector("#" + tree.fragment);
          if (element) {
            element.scrollIntoView(element);
          }
        }

      }

    });
  }

}
