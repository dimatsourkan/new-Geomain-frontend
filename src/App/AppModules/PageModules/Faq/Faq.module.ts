import { FaqComponent } from './Faq.component';
import { NgModule } from '@angular/core';
import { FAQ_ROUTING } from './Faq.routing';
import { AccordionModule } from 'ngx-accordion';
import { FaqNumberComponent } from './Components/Number/Number.component';
import { FaqNameComponent } from './Components/Name/Name.component';
import { FaqAdvertisingComponent } from './Components/Advertising/Advertising.component';
import { FaqNavigationComponent } from './Components/Navigation/Navigation.component';
import { FaqOrganizationComponent } from './Components/Organization/Organization.component';
import { FaqPrivacyComponent } from './Components/Privacy/Privacy.component';
import { FaqRegistrarsComponent } from './Components/Registrars/Registrars.component';
import { FaqSecurityComponent } from './Components/Security/Security.component';
import { FaqTrademarkPolicyComponent } from './Components/TrademarkPolicy/TrademarkPolicy.component';
import { FaqValidationComponent } from './Components/Validation/Validation.component';
import { FaqWhoisRecordsComponent } from './Components/WhoisRecords/WhoisRecords.component';
import { ScrollToTopModule } from '../../../BaseModules/ScrollToTop/ScrollToTop.module';



@NgModule({
  imports : [
    AccordionModule,
    ScrollToTopModule,
    FAQ_ROUTING
  ],
  declarations : [
    FaqComponent,
    FaqNameComponent,
    FaqNumberComponent,
    FaqPrivacyComponent,
    FaqSecurityComponent,
    FaqRegistrarsComponent,
    FaqValidationComponent,
    FaqNavigationComponent,
    FaqAdvertisingComponent,
    FaqWhoisRecordsComponent,
    FaqOrganizationComponent,
    FaqTrademarkPolicyComponent,
  ],
  exports : [
  ],
  providers : []
})

export class FaqModule {
}
