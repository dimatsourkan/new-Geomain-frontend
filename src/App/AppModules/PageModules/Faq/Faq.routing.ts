import { RouterModule } from '@angular/router';
import { FaqComponent } from './Faq.component';
import { FaqNumberComponent } from './Components/Number/Number.component';
import { FaqNameComponent } from './Components/Name/Name.component';
import { FaqAdvertisingComponent } from './Components/Advertising/Advertising.component';
import { FaqNavigationComponent } from './Components/Navigation/Navigation.component';
import { FaqOrganizationComponent } from './Components/Organization/Organization.component';
import { FaqPrivacyComponent } from './Components/Privacy/Privacy.component';
import { FaqRegistrarsComponent } from './Components/Registrars/Registrars.component';
import { FaqSecurityComponent } from './Components/Security/Security.component';
import { FaqTrademarkPolicyComponent } from './Components/TrademarkPolicy/TrademarkPolicy.component';
import { FaqValidationComponent } from './Components/Validation/Validation.component';
import { FaqWhoisRecordsComponent } from './Components/WhoisRecords/WhoisRecords.component';



export const routes = [
  {
    path : '',
    component : FaqComponent,
    children : [
      {
        path : '',
        pathMatch: 'full',
        redirectTo : '/faq/number'
      },
      {
        path : 'number',
        component : FaqNumberComponent
      },
      {
        path : 'name',
        component : FaqNameComponent
      },
      {
        path : 'advertising',
        component : FaqAdvertisingComponent
      },
      {
        path : 'navigation',
        component : FaqNavigationComponent
      },
      {
        path : 'organization',
        component : FaqOrganizationComponent
      },
      {
        path : 'privacy',
        component : FaqPrivacyComponent
      },
      {
        path : 'registrars',
        component : FaqRegistrarsComponent
      },
      {
        path : 'security',
        component : FaqSecurityComponent
      },
      {
        path : 'trademark-policy',
        component : FaqTrademarkPolicyComponent
      },
      {
        path : 'validation',
        component : FaqValidationComponent
      },
      {
        path : 'whois-records',
        component : FaqWhoisRecordsComponent
      }
    ]
  }
];

export const FAQ_ROUTING = RouterModule.forChild(routes);