import { Component, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../../../../../BaseClasses/Components/Base.component';
import { BehaviorSubject } from 'rxjs';
import { IVisitCard } from '../../../../../../../EntityModules/VisitCard/VisitCard.model';
import { DataStore } from '../../../../../../../EntityModules/DataStore/data.store';
import { ConfirmModal } from '../../../../../../../BaseModules/Modal/Components/Confirm/Confirm.component';
import { VisitCardService } from '../../../../../../../EntityModules/VisitCard/VisitCard.service';
import { Router } from '@angular/router';



@Component({
  selector : 'visit-card-info',
  templateUrl : './Info.component.html',
  styleUrls : ['./Info.component.less']
})

export class VisitCardInfoComponent extends BaseComponent {

  @ViewChild('confirm') private confirm : ConfirmModal;

  card$ : BehaviorSubject<IVisitCard> = DataStore.visitCard.item;

  constructor(
    private visitCardService : VisitCardService,
    private router : Router
  ) {
    super()
  }

  deleteCard() {

    this.confirm.open(() => {
      this.visitCardService.remove(this.card$.value).subscribe(
        () => {
          this.router.navigate(['/business-cards', 'my']);
        }
      )
    });

  }
}
