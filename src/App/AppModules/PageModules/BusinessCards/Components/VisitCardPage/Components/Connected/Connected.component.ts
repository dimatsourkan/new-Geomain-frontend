import { Component } from '@angular/core';
import { BaseComponent } from '../../../../../../../BaseClasses/Components/Base.component';



@Component({
  selector : 'visit-card-info',
  templateUrl : './Connected.component.html',
  styleUrls : ['./Connected.component.less']
})

export class VisitCardConnectedComponent extends BaseComponent {}
