import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IVisitCard } from '../../../../../EntityModules/VisitCard/VisitCard.model';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';



@Component({
  selector : 'visit-card-page',
  templateUrl : './VisitCardPage.component.html',
  styleUrls : ['./VisitCardPage.component.less']
})

export class VisitCardPageComponent {
  card$ : BehaviorSubject<IVisitCard> = DataStore.visitCard.item;
}
