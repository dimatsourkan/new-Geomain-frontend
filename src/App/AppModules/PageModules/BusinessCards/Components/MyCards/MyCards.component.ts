import { Component, ViewChild } from '@angular/core';
import { VisitCardService } from '../../../../../EntityModules/VisitCard/VisitCard.service';
import { BehaviorSubject } from 'rxjs';
import { IVisitCard } from '../../../../../EntityModules/VisitCard/VisitCard.model';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { Loader } from '../../../../../BaseModules/Loader/Loader.component';
import { finalize } from 'rxjs/operators';
import { IPagination, Pagination } from '../../../../../BaseClasses/Models/Pagination.model';



@Component({
  selector : 'my-cards',
  templateUrl : './MyCards.component.html',
  styleUrls : ['./MyCards.component.less']
})

export class MyCardsComponent {

  pagination : Pagination;

  @ViewChild('loader') private loader : Loader;
  list$ : BehaviorSubject<IVisitCard[]>;

  constructor(
    private visitCardService : VisitCardService
  ) {
    this.list$ = DataStore.visitCard.list;
    this.pagination = DataStore.visitCard.pagination.value;
    DataStore.visitCard.pagination.subscribe(p => this.pagination = p);
  }

  ngOnInit() {
    this.getCards();
  }

  getCards() {
    this.loader.show();
    this.visitCardService.my(this.paginationParams)
      .pipe(finalize(() => this.loader.hide()))
      .subscribe();
  }

  changePage(page : number) {
    this.pagination.page = page;
    this.getCards();
  }

  get paginationParams() {
    return this.pagination.queryParams;
  }
}
