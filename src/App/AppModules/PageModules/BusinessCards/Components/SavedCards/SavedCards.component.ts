import { Component, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IVisitCard } from '../../../../../EntityModules/VisitCard/VisitCard.model';
import { VisitCardService } from '../../../../../EntityModules/VisitCard/VisitCard.service';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { Loader } from '../../../../../BaseModules/Loader/Loader.component';
import { finalize } from 'rxjs/operators';
import { Pagination } from '../../../../../BaseClasses/Models/Pagination.model';



@Component({
  selector : 'saved-cards',
  templateUrl : './SavedCards.component.html',
  styleUrls : ['./SavedCards.component.less']
})

export class SavedCardsComponent {

  pagination : Pagination;

  @ViewChild('loader') private loader : Loader;
  list$ : BehaviorSubject<IVisitCard[]>;

  constructor(
    private visitCardService : VisitCardService
  ) {
    this.list$ = DataStore.visitCard.list;
    this.pagination = DataStore.visitCard.pagination.value;
    DataStore.visitCard.pagination.subscribe(p => this.pagination = p);
  }

  ngOnInit() {
    this.getCards();
  }

  getCards() {
    this.loader.show();
    this.visitCardService.collected(this.paginationParams)
      .pipe(finalize(() => this.loader.hide()))
      .subscribe();
  }

  changePage(page : number) {
    this.pagination.page = page;
    this.getCards();
  }

  get paginationParams() {
    return this.pagination.queryParams;
  }
}
