import { NgModule } from '@angular/core';
import { CARDS_ROUTING } from './BusinessCards.routing';
import { CommonModule } from '@angular/common';
import { LoaderModule } from '../../../BaseModules/Loader/Loader.module';
import { BusinessCardsComponent } from './BusinessCards.component';
import { MyCardsComponent } from './Components/MyCards/MyCards.component';
import { SavedCardsComponent } from './Components/SavedCards/SavedCards.component';
import { VisitCardModule } from '../../VisitCard/VisitCard.module';
import { VisitCardPageComponent } from './Components/VisitCardPage/VisitCardPage.component';
import { RouterModule } from '@angular/router';
import { VisitCardInfoComponent } from './Components/VisitCardPage/Components/Info/Info.component';
import { VisitCardConnectedComponent } from './Components/VisitCardPage/Components/Connected/Connected.component';
import { FormControlsModule } from '../../../BaseModules/FormControls/FormControls.module';
import { PaginationModule } from '../../../BaseModules/Pagination/Pagination.module';
import { ModalModule } from '../../../BaseModules/Modal/Modal.module';



@NgModule({
  imports : [
    CommonModule,
    LoaderModule,
    VisitCardModule,
    RouterModule,
    ModalModule,
    FormControlsModule,
    PaginationModule,
    CARDS_ROUTING
  ],
  declarations : [
    BusinessCardsComponent,
    MyCardsComponent,
    SavedCardsComponent,
    VisitCardPageComponent,
    VisitCardInfoComponent,
    VisitCardConnectedComponent
  ],
  exports : [
  ],
  providers : []
})

export class BusinessCardsModule {

}
