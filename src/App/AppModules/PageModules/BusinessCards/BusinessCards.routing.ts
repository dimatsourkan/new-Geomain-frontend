import { RouterModule } from '@angular/router';
import { BusinessCardsComponent } from './BusinessCards.component';
import { MyCardsComponent } from './Components/MyCards/MyCards.component';
import { SavedCardsComponent } from './Components/SavedCards/SavedCards.component';
import { VisitCardInfoComponent } from './Components/VisitCardPage/Components/Info/Info.component';
import { VisitCardConnectedComponent } from './Components/VisitCardPage/Components/Connected/Connected.component';
import { VisitCardPageComponent } from './Components/VisitCardPage/VisitCardPage.component';
import { VisitCardResolver } from '../../../EntityModules/VisitCard/VisitCard.resolver';



export const routes = [
  {
    path : '',
    component : BusinessCardsComponent,
    children : [
      {
        path : '',
        pathMatch: 'full',
        redirectTo : '/business-cards/my'
      },
      {
        path : 'my',
        component : MyCardsComponent
      },
      {
        path : 'saved',
        component : SavedCardsComponent
      }
    ]
  },
  {
    path : 'card/:id',
    component : VisitCardPageComponent,
    resolve : {
      card : VisitCardResolver
    },
    children : [
      {
        path : '',
        pathMatch: 'full',
        redirectTo : 'info'
      },
      {
        path : 'info',
        component : VisitCardInfoComponent
      },
      {
        path : 'connected',
        component : VisitCardConnectedComponent
      }
    ]
  }
];

export const CARDS_ROUTING = RouterModule.forChild(routes);