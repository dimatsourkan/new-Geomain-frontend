import { RouterModule } from '@angular/router';
import { TermsComponent } from './Terms.component';
import { PrivacyPolicyPrivacyComponent } from './Components/Privacy/Privacy.component';



export const routes = [
  {
    path : '',
    component : TermsComponent,
    children : [
      {
        path : '',
        component : PrivacyPolicyPrivacyComponent
      }
    ]
  }
];

export const TERMS_ROUTING = RouterModule.forChild(routes);