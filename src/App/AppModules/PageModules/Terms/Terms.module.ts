import { TermsComponent } from './Terms.component';
import { NgModule } from '@angular/core';
import { TERMS_ROUTING } from './Terms.routing';
import { AccordionModule } from 'ngx-accordion';
import { PrivacyPolicyPrivacyComponent } from './Components/Privacy/Privacy.component';
import { ScrollToTopModule } from '../../../BaseModules/ScrollToTop/ScrollToTop.module';



@NgModule({
  imports : [
    AccordionModule,
    ScrollToTopModule,
    TERMS_ROUTING
  ],
  declarations : [
    TermsComponent,
    PrivacyPolicyPrivacyComponent,
  ],
  exports : [
  ],
  providers : []
})

export class TermsModule {
}
