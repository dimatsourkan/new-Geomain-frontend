import { NgModule } from '@angular/core';
import { REGISTRAR_ROUTING } from './Registrar.routing';
import { RegistrarComponent } from './Registrar.component';



@NgModule({
  imports : [
    REGISTRAR_ROUTING
  ],
  declarations : [
    RegistrarComponent
  ],
  exports : [
  ],
  providers : []
})

export class RegistrarModule {

}
