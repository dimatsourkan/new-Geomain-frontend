import { RouterModule } from '@angular/router';
import { RegistrarComponent } from './Registrar.component';



export const routes = [
  {
    path : '',
    component : RegistrarComponent,
    children : [
      {
        path : '',
        pathMatch: 'full',
        redirectTo : '/registrar/dashboard'
      },
      {
        path : 'dashboard',
        loadChildren : './PageModules/Dashboard/Dashboard.module#DashboardModule',
      },
      {
        path : 'manage',
        loadChildren : './PageModules/Manage/Manage.module#ManageModule',
      },
      {
        path : 'register',
        loadChildren : './PageModules/Register/Register.module#RegisterModule',
      },
      {
        path : 'renew',
        loadChildren : './PageModules/Renew/Renew.module#RenewModule',
      },
      {
        path : 'account',
        loadChildren : './PageModules/Account/Account.module#AccountModule',
      }
    ]
  }
];

export const REGISTRAR_ROUTING = RouterModule.forChild(routes);