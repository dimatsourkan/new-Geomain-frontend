import { NgModule } from '@angular/core';
import { ACCOUNT_ROUTING } from './Account.routing';
import { CommonModule } from '@angular/common';
import { LoaderModule } from '../../../../../BaseModules/Loader/Loader.module';
import { FormControlsModule } from '../../../../../BaseModules/FormControls/FormControls.module';
import { FormsModule } from '@angular/forms';
import { AccountBasicComponent } from './Components/Basic/Basic.component';
import { AccountComponent } from './Account.component';
import { AccountDiscountCardComponent } from './Components/Basic/Components/DiscountCard/DiscountCard.component';
import { AccountBalanceCardComponent } from './Components/Basic/Components/BalanceCard/BalanceCard.component';
import { ProfileAvatarModule } from '../../../../ProfileAvatar/ProfileAvatar.module';
import { AccountManagmentomponent } from './Components/Managment/Managment.component';
import { PaginationModule } from '../../../../../BaseModules/Pagination/Pagination.module';
import { AccountInfoComponent } from './Components/Info/Info.component';



@NgModule({
  imports : [
    CommonModule,
    LoaderModule,
    FormsModule,
    FormControlsModule,
    ProfileAvatarModule,
    PaginationModule,
    ACCOUNT_ROUTING
  ],
  declarations : [
    AccountComponent,
    AccountDiscountCardComponent,
    AccountBalanceCardComponent,
    AccountBasicComponent,
    AccountManagmentomponent,
    AccountInfoComponent
  ],
  exports : [
  ],
  providers : []
})

export class AccountModule {

}
