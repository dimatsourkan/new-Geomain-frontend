import { RouterModule } from '@angular/router';
import { AccountBasicComponent } from './Components/Basic/Basic.component';
import { AccountComponent } from './Account.component';
import { AccountManagmentomponent } from './Components/Managment/Managment.component';
import { AccountInfoComponent } from './Components/Info/Info.component';



export const routes = [
  {
    path : '',
    component : AccountComponent,
    children : [
      {
        path : '',
        pathMatch : 'full',
        redirectTo : '/registrar/account/basic'
      },
      {
        path : 'basic',
        component : AccountBasicComponent
      },
      {
        path : 'managment',
        component : AccountManagmentomponent
      }
    ]
  },
  {
    path : 'info',
    component : AccountInfoComponent
  }
];

export const ACCOUNT_ROUTING = RouterModule.forChild(routes);