import { Component, } from '@angular/core';
import { BaseComponent } from '../../../../../../../BaseClasses/Components/Base.component';
import { IUser } from '../../../../../../../EntityModules/User/User.model';
import { DataStore } from '../../../../../../../EntityModules/DataStore/data.store';



@Component({
  selector : 'account-info',
  templateUrl : './Info.component.html',
  styleUrls : ['./Info.component.less']
})

export class AccountInfoComponent extends BaseComponent {
  user : IUser = DataStore.user.current.value;
}
