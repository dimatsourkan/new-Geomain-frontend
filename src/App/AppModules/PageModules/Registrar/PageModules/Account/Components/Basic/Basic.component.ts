import { Component, } from '@angular/core';
import { BaseComponent } from '../../../../../../../BaseClasses/Components/Base.component';
import { IUser } from '../../../../../../../EntityModules/User/User.model';
import { DataStore } from '../../../../../../../EntityModules/DataStore/data.store';



@Component({
  selector : 'account-basic',
  templateUrl : './Basic.component.html',
  styleUrls : ['./Basic.component.less']
})

export class AccountBasicComponent extends BaseComponent {
  user : IUser = DataStore.user.current.value;
}
