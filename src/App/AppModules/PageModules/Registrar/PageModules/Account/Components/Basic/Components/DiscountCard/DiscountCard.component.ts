import { Component } from '@angular/core';
import { BaseComponent } from '../../../../../../../../../BaseClasses/Components/Base.component';



@Component({
  selector : 'account-discount-card',
  templateUrl : './DiscountCard.component.html',
  styleUrls : ['./DiscountCard.component.less']
})

export class AccountDiscountCardComponent extends BaseComponent {

}
