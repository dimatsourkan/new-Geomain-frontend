import { Component } from '@angular/core';
import { BaseComponent } from '../../../../../../../../../BaseClasses/Components/Base.component';



@Component({
  selector : 'account-balance-card',
  templateUrl : './BalanceCard.component.html',
  styleUrls : ['./BalanceCard.component.less']
})

export class AccountBalanceCardComponent extends BaseComponent {

}
