import { NgModule } from '@angular/core';
import { REGISTER_ROUTING } from './Register.routing';
import { CommonModule } from '@angular/common';
import { LoaderModule } from '../../../../../BaseModules/Loader/Loader.module';
import { FormControlsModule } from '../../../../../BaseModules/FormControls/FormControls.module';
import { RouterModule } from '@angular/router';
import { SearchComponent } from './Components/Search/Search.component';
import { ItemComponent } from './Components/Item/Item.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports : [
    CommonModule,
    LoaderModule,
    RouterModule,
    FormsModule,
    FormControlsModule,
    REGISTER_ROUTING
  ],
  declarations : [
    SearchComponent,
    ItemComponent
  ],
  exports : [
  ],
  providers : []
})

export class RegisterModule {

}
