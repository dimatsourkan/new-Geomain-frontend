import { Component } from '@angular/core';



@Component({
  selector : 'item',
  templateUrl : './Item.component.html',
  styleUrls : ['./Item.component.less']
})

export class ItemComponent {
  rangeOptions = {
    min : 1,
    max : 10
  };

  years = 2;
}
