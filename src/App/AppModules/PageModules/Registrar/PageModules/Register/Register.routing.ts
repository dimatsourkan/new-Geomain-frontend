import { RouterModule } from '@angular/router';
import { SearchComponent } from './Components/Search/Search.component';
import { ItemComponent } from './Components/Item/Item.component';



export const routes = [
  {
    path : '',
    component : SearchComponent
  },
  {
    path : ':id',
    component : ItemComponent
  }
];

export const REGISTER_ROUTING = RouterModule.forChild(routes);