import { RouterModule } from '@angular/router';
import { ListComponent } from './Components/List/List.component';
import { ItemComponent } from './Components/Item/Item.component';



export const routes = [
  {
    path : '',
    pathMatch: 'full',
    redirectTo : '/registrar/manage/list'
  },
  {
    path : 'list',
    component : ListComponent
  },
  {
    path : ':id',
    component : ItemComponent
  }
];

export const MANAGE_ROUTING = RouterModule.forChild(routes);