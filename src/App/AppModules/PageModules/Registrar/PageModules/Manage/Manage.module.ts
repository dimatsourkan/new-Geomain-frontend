import { NgModule } from '@angular/core';
import { MANAGE_ROUTING } from './Manage.routing';
import { CommonModule } from '@angular/common';
import { ListComponent } from './Components/List/List.component';
import { LoaderModule } from '../../../../../BaseModules/Loader/Loader.module';
import { FormControlsModule } from '../../../../../BaseModules/FormControls/FormControls.module';
import { PaginationModule } from '../../../../../BaseModules/Pagination/Pagination.module';
import { ItemComponent } from './Components/Item/Item.component';
import { RouterModule } from '@angular/router';



@NgModule({
  imports : [
    CommonModule,
    LoaderModule,
    RouterModule,
    FormControlsModule,
    PaginationModule,
    MANAGE_ROUTING
  ],
  declarations : [
    ListComponent,
    ItemComponent
  ],
  exports : [
  ],
  providers : []
})

export class ManageModule {

}
