import { RouterModule } from '@angular/router';
import { FindComponent } from './Components/Find/Find.component';
import { ItemComponent } from './Components/Item/Item.component';



export const routes = [
  {
    path : '',
    pathMatch: 'full',
    redirectTo : '/registrar/renew/find'
  },
  {
    path : 'find',
    component : FindComponent
  },
  {
    path : ':id',
    component : ItemComponent
  }
];

export const RENEW_ROUTING = RouterModule.forChild(routes);