import { NgModule } from '@angular/core';
import { RENEW_ROUTING } from './Renew.routing';
import { CommonModule } from '@angular/common';
import { LoaderModule } from '../../../../../BaseModules/Loader/Loader.module';
import { FormControlsModule } from '../../../../../BaseModules/FormControls/FormControls.module';
import { PaginationModule } from '../../../../../BaseModules/Pagination/Pagination.module';
import { RouterModule } from '@angular/router';
import { FindComponent } from './Components/Find/Find.component';
import { ItemComponent } from './Components/Item/Item.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports : [
    CommonModule,
    LoaderModule,
    RouterModule,
    FormsModule,
    FormControlsModule,
    PaginationModule,
    RENEW_ROUTING
  ],
  declarations : [
    FindComponent,
    ItemComponent
  ],
  exports : [
  ],
  providers : []
})

export class RenewModule {

}
