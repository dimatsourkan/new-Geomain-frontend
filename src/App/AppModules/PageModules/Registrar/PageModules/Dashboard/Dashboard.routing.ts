import { RouterModule } from '@angular/router';
import { DashboardComponent } from './Dashboard.component';



export const routes = [
  {
    path : '',
    component : DashboardComponent
  }
];

export const DASHBOARD_ROUTING = RouterModule.forChild(routes);