import { Component, ViewChild } from '@angular/core';
import { RegistrarService } from '../../../../../EntityModules/User/Users.registrar.service';
import { Loader } from '../../../../../BaseModules/Loader/Loader.component';
import { finalize } from 'rxjs/internal/operators';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';



@Component({
  selector : 'dashboard',
  templateUrl : './Dashboard.component.html',
  styleUrls : ['./Dashboard.component.less']
})

export class DashboardComponent {

  @ViewChild('loader') private loader : Loader;

  data : any;

  chartData : number[];

  user$ = DataStore.user.current;

  constructor(
    private registrarService : RegistrarService
  ) {

  }

  getData() {
    this.loader.show();
    this.registrarService.dashboardSummary('today')
      .pipe(finalize(() => this.loader.hide()))
      .subscribe(res => {
      this.data = res;
      this.chartData = [
        parseFloat(this.data.countNumberGeomain),
        parseFloat(this.data.amountDebitedAddons)
      ];
    });
  }

  ngOnInit() {
    this.getData();
  }
}
