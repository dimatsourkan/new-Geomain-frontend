import { Component, OnInit } from '@angular/core';
import { DataStore } from '../../../../../../../EntityModules/DataStore/data.store';
import { RegistrarService } from '../../../../../../../EntityModules/User/Users.registrar.service';

@Component({
  selector : 'dashboard-geonames',
  templateUrl : './Geonames.component.html',
  styleUrls : ['./Geonames.component.less']
})

export class DashboardgeonamesComponent implements OnInit {

  geonames$ = DataStore.geoname.my;

  constructor(
    private registrarService : RegistrarService
  ) {
  }

  getMyGeonames() {
    this.registrarService.getMyGeonames().subscribe();
  }

  ngOnInit() {
    this.getMyGeonames();
  }
}
