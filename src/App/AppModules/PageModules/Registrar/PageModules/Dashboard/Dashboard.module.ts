import { NgModule } from '@angular/core';
import { DASHBOARD_ROUTING } from './Dashboard.routing';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './Dashboard.component';
import { LoaderModule } from '../../../../../BaseModules/Loader/Loader.module';
import { FormControlsModule } from '../../../../../BaseModules/FormControls/FormControls.module';
import { ChartModule } from '../../../../../BaseModules/Chart/Chart.module';
import { LineChartModule } from '../../../../../BaseModules/LineChart/LineChart.module';
import { DashboardgeonamesComponent } from './Components/Geonames/Geonames.component';



@NgModule({
  imports : [
    CommonModule,
    LoaderModule,
    FormControlsModule,
    ChartModule,
    LineChartModule,
    DASHBOARD_ROUTING
  ],
  declarations : [
    DashboardComponent,
    DashboardgeonamesComponent
  ],
  exports : [
  ],
  providers : []
})

export class DashboardModule {

}
