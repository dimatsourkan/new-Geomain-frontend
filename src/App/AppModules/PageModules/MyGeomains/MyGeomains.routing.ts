import { RouterModule } from '@angular/router';
import { MyGeomainsComponent } from './MyGeomains.component';
import { GeospotsComponent } from './Components/Geoposts/Geoposts.component';
import { GeonamesComponent } from './Components/Geonames/Geonames.component';



export const routes = [
  {
    path : '',
    component : MyGeomainsComponent,
    children : [
      {
        path : '',
        pathMatch: 'full',
        redirectTo : '/my-geomains/geospots'
      },
      {
        path : 'geospots',
        component : GeospotsComponent
      },
      {
        path : 'geonames',
        component : GeonamesComponent
      }
    ]
  }
];

export const MY_GEOMAINS_ROUTING = RouterModule.forChild(routes);