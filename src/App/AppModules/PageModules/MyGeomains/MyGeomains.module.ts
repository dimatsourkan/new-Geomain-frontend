import { NgModule } from '@angular/core';
import { SearchModule } from '../../Search/Search.module';
import { MY_GEOMAINS_ROUTING } from './MyGeomains.routing';
import { CommonModule } from '@angular/common';
import { ModalModule } from '../../../BaseModules/Modal/Modal.module';
import { LoaderModule } from '../../../BaseModules/Loader/Loader.module';
import { FormControlsModule } from '../../../BaseModules/FormControls/FormControls.module';
import { GeonamesComponent } from './Components/Geonames/Geonames.component';
import { MyGeomainsComponent } from './MyGeomains.component';
import { GeospotsComponent } from './Components/Geoposts/Geoposts.component';
import { GeomainComponentsModule } from '../../Geomain/Geomain.module';



@NgModule({
  imports : [
    CommonModule,
    SearchModule,
    ModalModule,
    LoaderModule,
    FormControlsModule,
    GeomainComponentsModule,
    MY_GEOMAINS_ROUTING
  ],
  declarations : [
    GeonamesComponent,
    GeospotsComponent,
    MyGeomainsComponent,
  ],
  exports : [
  ],
  providers : []
})

export class MyGeomainsModule {

}
