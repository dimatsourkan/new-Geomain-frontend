import { Component, OnInit, ViewChild } from '@angular/core';
import { GeomainService } from '../../../../../EntityModules/Geomain/Geomain.service';
import { Loader } from '../../../../../BaseModules/Loader/Loader.component';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { Observable } from 'rxjs/Rx';
import { IGeomain } from '../../../../../EntityModules/Geomain/Geomain.model';
import { finalize } from 'rxjs/operators';



@Component({
  selector : 'geoposts',
  templateUrl : './Geoposts.component.html',
  styleUrls : ['./Geoposts.component.less']
})

export class GeospotsComponent implements OnInit {

  @ViewChild('loader') private loader : Loader;

  list$ : Observable<IGeomain[]>;

  constructor(private geoNumberService : GeomainService) {

    this.list$ = DataStore.geomain.list;

  }

  ngOnInit() {

    if(!DataStore.geomain.list.value) {
      this.loader.show();
    }

    this.geoNumberService.list()
      .pipe(finalize(() => this.loader.hide()))
      .subscribe();
  }

}
