import { Component, OnInit, ViewChild } from '@angular/core';
import { Loader } from '../../../../../BaseModules/Loader/Loader.component';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { IGeomain } from '../../../../../EntityModules/Geomain/Geomain.model';
import { Observable } from 'rxjs/Rx';
import { GeonameService } from '../../../../../EntityModules/Geomain/Geoname.service';



@Component({
  selector : 'geonames',
  templateUrl : './Geonames.component.html',
  styleUrls : ['./Geonames.component.less']
})

export class GeonamesComponent implements OnInit {

  @ViewChild('loader') private loader : Loader;

  list$ : Observable<IGeomain[]>;

  constructor(private geonameService : GeonameService) {
    this.list$ = DataStore.geoname.list;
  }

  ngOnInit() {
    this.loader.show();
    this.geonameService.list().subscribe(res => {
      this.loader.hide();
    }, () => this.loader.hide());
  }
}
