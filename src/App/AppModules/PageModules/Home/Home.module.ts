import { HomeComponent } from './Home.component';
import { NgModule } from '@angular/core';
import { SearchModule } from '../../Search/Search.module';



@NgModule({
  imports : [
    SearchModule
  ],
  declarations : [
    HomeComponent
  ],
  exports : [
    HomeComponent
  ],
  providers : []
})

export class HomeModule {
}
