import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Helpers } from '../../../BaseClasses/Helpers';
declare var $ : any;


@Component({
  selector : 'home',
  templateUrl : './Home.component.html',
  styleUrls : ['./Home.component.less']
})


export class HomeComponent implements AfterViewInit {

  @ViewChild('slider') slider : ElementRef;

  helpers = Helpers;

  constructor(private router : Router) {

  }

  ngAfterViewInit() {
    this.initSlider();
  }

  onSuccess(e : any) {
    if(e.result.data.name) {
      this.router.navigate(['/gn', e.result.data.name]);
    } else {
      this.router.navigate(['/gs', e.result.data.number]);
    }
  }

  initSlider() {
    $(this.slider.nativeElement).slick({
      slidesToShow : 1,
      arrows : false,
      pauseOnHover : false,
      autoplay : true,
      autoplaySpeed : 5000,
      dots : true,
      customPaging: function(slider, i) {
        return $('<button type="button" />');
      },
    });
  }
}
