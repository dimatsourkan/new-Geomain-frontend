import { SupportComponent } from './Support.component';
import { NgModule } from '@angular/core';
import { SUPPORT_ROUTING } from './Support.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormControlsModule } from '../../../BaseModules/FormControls/FormControls.module';
import { ValidationModule } from '../../../BaseModules/Validation/Validation.module';
import { LoaderModule } from '../../../BaseModules/Loader/Loader.module';
import { NotificationsModule } from '../../../BaseModules/Notifications/Notifications.module';
import { RouterModule } from '@angular/router';



@NgModule({
  imports : [
    SUPPORT_ROUTING,
    FormsModule,
    ReactiveFormsModule,
    FormControlsModule,
    ValidationModule,
    LoaderModule,
    NotificationsModule,
    RouterModule
  ],
  declarations : [
    SupportComponent
  ],
  exports : [
  ],
  providers : []
})

export class SupportModule {
}
