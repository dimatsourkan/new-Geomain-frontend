import { Component, ViewChild } from '@angular/core';
import { UserService } from '../../../EntityModules/User/Users.service';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthorizationService } from '../../../BaseModules/Authorization/Authorization.service';
import { ValidatorService } from '../../../BaseModules/Validation/Validation.service';
import { Loader } from '../../../BaseModules/Loader/Loader.component';
import { DataStore } from '../../../EntityModules/DataStore/data.store';
import { NotificationsService } from '../../../BaseModules/Notifications/Notifications.service';
import { Notification } from '../../../BaseModules/Notifications/Notifications.model';
import { NOTIFICATION_TYPE } from '../../../BaseModules/Notifications/Notifications.const';
import { Router } from '@angular/router';

export interface ISupportData {
  topic : string;
  message : string;
  email : string;
  status : string;
  priority : string;
}

@Component({
  selector : 'support',
  templateUrl : './Support.component.html',
  styleUrls : ['./Support.component.less']
})

export class SupportComponent {

  @ViewChild('loader') private loader : Loader;

  form : FormGroup;

  constructor(
    private userService : UserService,
    private validationService : ValidatorService,
    private notificationsService : NotificationsService,
    private router : Router,
    public authorizationService : AuthorizationService
  ) {

    this.form = new FormGroup({
      topic    : new FormControl(''),
      message  : new FormControl(''),
      email    : new FormControl(DataStore.user.current.value.email),
      status   : new FormControl('high'),
      priority : new FormControl('high'),
    });

  }

  submit() {
    this.loader.show();
    this.userService.support(this.form.value).subscribe(
      () => {
        this.loader.hide();
        this.onSuccess();
      },
      err => {
        this.loader.hide();
        if(err.status === 400) {
          this.validationService.addErrorToForm(this.form, err.error);
        } else {
          this.onError();
        }
      }
    );
  }

  onSuccess() {
    this.notificationsService.push(new Notification({
      title : 'Ok',
      message : 'Your application was successfully accepted',
      type : NOTIFICATION_TYPE.SUCCESS
    }));
    this.router.navigate(['/']);
  }

  onError() {
    this.notificationsService.push(new Notification({
      title : 'Error',
      message : 'Oops, something went wrong',
      type : NOTIFICATION_TYPE.ERROR
    }))
  }

}
