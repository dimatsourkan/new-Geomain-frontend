import { RouterModule } from '@angular/router';
import { SupportComponent } from './Support.component';



export const routes = [
  {
    path : '',
    component : SupportComponent
  }
];

export const SUPPORT_ROUTING = RouterModule.forChild(routes);