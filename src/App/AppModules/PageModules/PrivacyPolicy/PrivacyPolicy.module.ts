import { PrivacyPolicyComponent } from './PrivacyPolicy.component';
import { NgModule } from '@angular/core';
import { PRIVACY_POLICY_ROUTING } from './PrivacyPolicy.routing';
import { AccordionModule } from 'ngx-accordion';
import { PrivacyPolicyPrivacyComponent } from './Components/Privacy/Privacy.component';
import { ScrollToTopModule } from '../../../BaseModules/ScrollToTop/ScrollToTop.module';



@NgModule({
  imports : [
    AccordionModule,
    ScrollToTopModule,
    PRIVACY_POLICY_ROUTING
  ],
  declarations : [
    PrivacyPolicyComponent,
    PrivacyPolicyPrivacyComponent,
  ],
  exports : [
  ],
  providers : []
})

export class PrivacyPolicyModule {
}
