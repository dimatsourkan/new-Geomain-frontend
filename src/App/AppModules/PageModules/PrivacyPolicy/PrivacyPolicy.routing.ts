import { RouterModule } from '@angular/router';
import { PrivacyPolicyComponent } from './PrivacyPolicy.component';
import { PrivacyPolicyPrivacyComponent } from './Components/Privacy/Privacy.component';



export const routes = [
  {
    path : '',
    component : PrivacyPolicyComponent,
    children : [
      {
        path : '',
        pathMatch: 'full',
        redirectTo : '/privacy-policy/privacy'
      },
      {
        path : 'privacy',
        component : PrivacyPolicyPrivacyComponent
      }
    ]
  }
];

export const PRIVACY_POLICY_ROUTING = RouterModule.forChild(routes);