import { AboutComponent } from './About.component';
import { NgModule } from '@angular/core';
import { ABOUT_ROUTING } from './About.routing';



@NgModule({
  imports : [
    ABOUT_ROUTING
  ],
  declarations : [
    AboutComponent
  ],
  exports : [
  ],
  providers : []
})

export class AboutModule {
}
