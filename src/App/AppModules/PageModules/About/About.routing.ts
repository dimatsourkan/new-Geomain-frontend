import { RouterModule } from '@angular/router';
import { AboutComponent } from './About.component';



export const routes = [
  {
    path : '',
    component : AboutComponent
  }
];

export const ABOUT_ROUTING = RouterModule.forChild(routes);