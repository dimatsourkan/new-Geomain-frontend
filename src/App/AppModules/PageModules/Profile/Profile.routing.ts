import { RouterModule } from '@angular/router';
import { ProfileBasicComponent } from './Components/Basic/Basic.component';
import { ProfileSecurityComponent } from './Components/Security/Security.component';
import { ProfileComponent } from './Profile.component';



export const routes = [
  {
    path : '',
    component : ProfileComponent,
    children : [
      {
        path : '',
        pathMatch: 'full',
        redirectTo : '/profile/basic'
      },
      {
        path : 'basic',
        component : ProfileBasicComponent
      },
      {
        path : 'security',
        component : ProfileSecurityComponent
      }
    ]
  }
];

export const PROFILE_ROUTING = RouterModule.forChild(routes);