import { ProfileBasicComponent } from './Components/Basic/Basic.component';
import { NgModule } from '@angular/core';
import { SearchModule } from '../../Search/Search.module';
import { PROFILE_ROUTING } from './Profile.routing';
import { ProfileSecurityComponent } from './Components/Security/Security.component';
import { CommonModule } from '@angular/common';
import { ModalModule } from '../../../BaseModules/Modal/Modal.module';
import { LoaderModule } from '../../../BaseModules/Loader/Loader.module';
import { FormControlsModule } from '../../../BaseModules/FormControls/FormControls.module';
import { ProfileComponent } from './Profile.component';
import { ProfileAvatarModule } from '../../ProfileAvatar/ProfileAvatar.module';



@NgModule({
  imports : [
    CommonModule,
    SearchModule,
    ModalModule,
    LoaderModule,
    FormControlsModule,
    ProfileAvatarModule,
    PROFILE_ROUTING
  ],
  declarations : [
    ProfileComponent,
    ProfileBasicComponent,
    ProfileSecurityComponent
  ],
  exports : [
  ],
  providers : []
})

export class ProfileModule {

}
