import { Component } from '@angular/core';
import { BaseComponent } from '../../../../../BaseClasses/Components/Base.component';
import { IUser } from '../../../../../EntityModules/User/User.model';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';



@Component({
  selector : 'profile-basic',
  templateUrl : './Basic.component.html',
  styleUrls : ['./Basic.component.less']
})

export class ProfileBasicComponent extends BaseComponent {

  user : IUser;

  constructor() {
    super();
    this.user = DataStore.user.current.value;
    DataStore.user.current.subscribe(user => this.user = user);
  }

}
