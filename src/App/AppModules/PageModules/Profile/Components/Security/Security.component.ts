import { Component } from '@angular/core';
import { DataStore } from '../../../../../EntityModules/DataStore/data.store';
import { IUser } from '../../../../../EntityModules/User/User.model';



@Component({
  selector : 'profile-security',
  templateUrl : './Security.component.html',
  styleUrls : ['./Security.component.less']
})

export class ProfileSecurityComponent {

  user : IUser;

  constructor() {
    this.user = DataStore.user.current.value;
    DataStore.user.current.subscribe(user => this.user = user);
  }
}
