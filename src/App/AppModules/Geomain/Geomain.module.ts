import { GeospotComponent } from './Components/Geospot/Geospot.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeonameComponent } from './Components/Geoname/Geoname.component';
import { RouterModule } from '@angular/router';
import { PhotosComponent } from './Components/Photos/Photos.component';
import { VideosComponent } from './Components/Video/Video.component';
import { FormControlsModule } from '../../BaseModules/FormControls/FormControls.module';
import { ModalModule } from '../../BaseModules/Modal/Modal.module';
import { ShareComponent } from './Components/Share/Share.component';
import { GeomainDeleteComponent } from './Components/Delete/Delete.component';
import { LoaderModule } from '../../BaseModules/Loader/Loader.module';
import { WorkingHoursComponent } from './Components/WorkingHours/WorkingHours.component';
import { FormsModule } from '@angular/forms';
import { ClipboardModule } from '../../BaseModules/Clipboard/Clipboard.module';
import { NotificationsModule } from '../../BaseModules/Notifications/Notifications.module';
import {ShareButtonsModule} from "ngx-sharebuttons";



@NgModule({
  imports : [
    FormsModule,
    FormControlsModule,
    CommonModule,
    RouterModule,
    LoaderModule,
    FormControlsModule,
    ModalModule,
    NotificationsModule,
    ClipboardModule,
    ShareButtonsModule.forRoot(),
  ],
  declarations : [
    ShareComponent,
    GeospotComponent,
    GeonameComponent,
    PhotosComponent,
    VideosComponent,
    GeomainDeleteComponent,
    WorkingHoursComponent
  ],
  exports : [
    ShareComponent,
    GeospotComponent,
    GeonameComponent,
    PhotosComponent,
    VideosComponent,
    GeomainDeleteComponent,
    WorkingHoursComponent
  ],
  providers : []
})

export class GeomainComponentsModule {
}
