import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { Schedule } from '../../../../EntityModules/Geomain/Geomain-schedule.model';
import { IGeomain } from '../../../../EntityModules/Geomain/Geomain.model';




@Component({
  selector : 'working-hours',
  templateUrl : './WorkingHours.component.html',
  styleUrls : ['./WorkingHours.component.less']
})

export class WorkingHoursComponent implements OnChanges {

  @Output() onUpdate = new EventEmitter<any>();

  @Input() geomain : IGeomain;

  schedule : Schedule;

  options = { alias: 'hh:mm'};

  constructor() {

  }

  ngOnChanges() {
    this.schedule = this.geomain.schedule;
  }

}
