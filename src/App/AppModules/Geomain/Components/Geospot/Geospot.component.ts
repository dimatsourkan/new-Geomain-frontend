import { Component, Input } from '@angular/core';
import { BaseComponent } from '../../../../BaseClasses/Components/Base.component';
import { IGeomain } from '../../../../EntityModules/Geomain/Geomain.model';
import { VISITOR_POLICY } from '../../../../EntityModules/Geomain/visitorPolicy';



@Component({
  selector : 'geospot',
  templateUrl : './Geospot.component.html',
  styleUrls : [
    './Geospot.component.less',
    './../../Geomain.component.less',
  ]
})

export class GeospotComponent extends BaseComponent {

  VISITOR_POLICY = VISITOR_POLICY;

  @Input() geomain : IGeomain;

}
