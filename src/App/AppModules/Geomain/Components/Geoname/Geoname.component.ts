import { Component, Input } from '@angular/core';
import { BaseComponent } from '../../../../BaseClasses/Components/Base.component';
import { IGeomain } from '../../../../EntityModules/Geomain/Geomain.model';



@Component({
  selector : 'geoname',
  templateUrl : './Geoname.component.html',
  styleUrls : [
    './../../Geomain.component.less',
    './Geoname.component.less',
  ]
})

export class GeonameComponent extends BaseComponent {
  @Input() geoname : IGeomain;
}
