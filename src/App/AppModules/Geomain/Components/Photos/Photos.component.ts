import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../../BaseClasses/Components/Base.component';
import { IGeomain } from '../../../../EntityModules/Geomain/Geomain.model';
import { GeomainService } from '../../../../EntityModules/Geomain/Geomain.service';
import { Loader } from '../../../../BaseModules/Loader/Loader.component';
import { ConfirmModal } from '../../../../BaseModules/Modal/Components/Confirm/Confirm.component';
import { DataStore } from '../../../../EntityModules/DataStore/data.store';
import { finalize } from 'rxjs/internal/operators';
import { Observable } from 'rxjs/Rx';
import { GeomainImage } from '../../../../EntityModules/Geomain/Geomain-image.model';
declare var $ : any;



@Component({
  selector : 'photos',
  templateUrl : './Photos.component.html',
  styleUrls : [
    './Photos.component.less',
  ]
})

export class PhotosComponent extends BaseComponent implements AfterViewInit, OnInit {

  @Input() geomain : IGeomain;
  @ViewChild('slider') slider : ElementRef;
  @ViewChild('loader') loader : Loader;
  @ViewChild('loadLoader') loadLoader : Loader;
  @ViewChild('confirm') confirm : ConfirmModal;

  imageList$ : Observable<GeomainImage[]>;

  constructor(
    private geomainService : GeomainService
  ) {
    super();
    this.imageList$ = DataStore.geomain.images;
  }

  ngOnInit() {
    this.getImages().subscribe(() => {
      this.initSlider();
    });
  }

  ngAfterViewInit() {
    this.initPlugins();
  }

  initPlugins() {
    /**
     * Инициализация fancybox для картинок
     */
    $('.photo-list [data-fancybox="photo"]').fancybox();

    /**
     * Костыль (более правильного решения не нашел) - после инициализации слайдера, добавляет исходным картинкам класс
     * В дальнейшем по этому классу будут удаляться элементы, т.к. Angular после инициализации слайдера
     * перестает их отслеживать
     */
    $(this.slider.nativeElement).on('init', () => {
      $(this.slider.nativeElement).find('.slick-slide').addClass('after-init');
    });

    /**
     * Костыль для показа попапа подтверждения удаления
     * Из-за специфической работы слайдера, нужен такой костыль
     * Если навесить (click) в шаблоне, то он будет работать не на всех слайдах
     */
    $(this.slider.nativeElement).on('click', '.remove-btn', e => {
      this.deleteImage($(e.currentTarget).data('id'));
    });
  }

  destroySlider() {
    if($(this.slider.nativeElement).hasClass('slick-initialized')) {
      $(this.slider.nativeElement).slick('unslick');
      /**
       * Удаляются элементы которые не отслеживает Angular после инициализации слайдера
       */
      $(this.slider.nativeElement).find('.after-init').remove();
    }
  }

  initSlider() {
    $(this.slider.nativeElement).slick({
      variableWidth : true,
      // infinite : false,
      slidesToShow : 3,
      prevArrow : '<button type="button" class="slick-prev"></button>',
      nextArrow : '<button type="button" class="slick-next"></button>'
    });
  }

  reinitSlider() {
    /**
     * setTimeout добавлен как костыль, без него инициализация слайдера проходит до того как Angular отрендерит
     * слайды с картинками
     */
    setTimeout(() => {
      this.destroySlider();
      this.initSlider();
    }, 1);
  }

  getImages() {
    this.loadLoader.show();
    return this.geomainService.getImages(this.geomain.number).pipe(
      finalize(() => {
        this.loadLoader.hide();
        this.reinitSlider();
      }));
  }

  uploadImage(form : ElementRef) {
    this.loadLoader.show();
    this.geomainService.uploadImage(this.geomain.number, form).subscribe(() => {
      this.loadLoader.hide();
      this.reinitSlider();
    }, () => this.loadLoader.hide());
  }

  deleteImage(id : number) {
    this.confirm.open(() => {
      this.loader.show();
      this.geomainService.deleteImage(id).subscribe(() => {
        this.loader.hide();
        this.confirm.close();
        this.reinitSlider();
      }, () => this.loader.hide());
    })
  }
}
