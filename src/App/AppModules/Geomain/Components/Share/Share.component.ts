import { Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from '../../../../BaseClasses/Components/Base.component';
import { IGeomain } from '../../../../EntityModules/Geomain/Geomain.model';
import { Modal } from '../../../../BaseModules/Modal/Components/Modal/Modal.component';
import { NotificationsService } from '../../../../BaseModules/Notifications/Notifications.service';
import { Notification } from '../../../../BaseModules/Notifications/Notifications.model';
import { NOTIFICATION_TYPE } from '../../../../BaseModules/Notifications/Notifications.const';



@Component({
  selector : 'share-geomain',
  templateUrl : './Share.component.html',
  encapsulation : ViewEncapsulation.None,
  styleUrls : [
    './Share.component.less',
  ]
})

export class ShareComponent extends BaseComponent {

  @Input() geomain : IGeomain;
  @ViewChild('modal') private modal : Modal;

  constructor(private notificationsService : NotificationsService) {
    super();
  }

  get href() {
    return document.location.href;
  }

  open() {
    this.modal.open();
  }

  onCopy() {
    this.notificationsService.push(
      new Notification({
        title : 'Ok',
        message : 'Link copied',
        type : NOTIFICATION_TYPE.SUCCESS
      })
    )
  }
}
