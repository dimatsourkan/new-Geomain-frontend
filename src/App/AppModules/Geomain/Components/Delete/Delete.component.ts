import { Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from '../../../../BaseClasses/Components/Base.component';
import { IGeomain } from '../../../../EntityModules/Geomain/Geomain.model';
import { Modal } from '../../../../BaseModules/Modal/Components/Modal/Modal.component';



@Component({
  selector : 'delete-geomain',
  templateUrl : './Delete.component.html',
  encapsulation : ViewEncapsulation.None,
  styleUrls : [
    './Delete.component.less',
  ]
})

export class GeomainDeleteComponent extends BaseComponent {

  @Input() title : string;
  @Input() geomain : IGeomain;
  @ViewChild('modal') private modal : Modal;

  deleteCallback : Function = () => {};

  open(callback : Function = () => {}) {
    this.modal.open();
    this.deleteCallback = callback;
  }

  close() {
    this.modal.close();
  }
}
