import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../BaseClasses/Components/Base.component';
import { IGeomain } from '../../../../EntityModules/Geomain/Geomain.model';
import { GeomainService } from '../../../../EntityModules/Geomain/Geomain.service';



@Component({
  selector : 'videos',
  templateUrl : './Video.component.html',
  styleUrls : [
    './Video.component.less',
  ]
})

export class VideosComponent extends BaseComponent implements OnInit {

  @Input() geomain : IGeomain;

  constructor(private geomainService : GeomainService) {
    super();
  }

  ngOnInit() {
    // this.getAdvertising();
  }

  getAdvertising() {
    this.geomainService.getAdvertising(this.geomain.id).subscribe();
  }

  uploadImage(form : ElementRef) {
    this.geomainService.uploadAdVideo(this.geomain.number, form).subscribe();
  }
}
