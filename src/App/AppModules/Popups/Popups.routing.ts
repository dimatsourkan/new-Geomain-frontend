import { Routes } from '@angular/router';
import { NotAuthenticated } from '../../BaseModules/Authorization/CanActivate/NotAuthenticated';
import { LoginComponent } from './Components/Login/Login.component';
import { ForgotComponent } from './Components/Forgot/Forgot.component';
import { ChangePassComponent } from './Components/ChangePass/ChangePass.component';



export const POPUPS_ROUTES : Routes = [
  {
    path : 'login',
    canActivate : [NotAuthenticated],
    component : LoginComponent,
    outlet : 'auth'
  },
  {
    path : 'forgot',
    canActivate : [NotAuthenticated],
    component : ForgotComponent,
    outlet : 'auth'
  },
  {
    path : 'change-pass/:token',
    canActivate : [NotAuthenticated],
    component : ChangePassComponent,
    outlet : 'auth'
  }
];
