import { AfterViewInit, Component, ViewChild } from '@angular/core';
import {BaseComponent} from "../../../../BaseClasses/Components/Base.component";
import { Modal } from '../../../../BaseModules/Modal/Components/Modal/Modal.component';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { Loader } from '../../../../BaseModules/Loader/Loader.component';
import { AuthorizationService } from '../../../../BaseModules/Authorization/Authorization.service';
import { ValidatorService } from '../../../../BaseModules/Validation/Validation.service';
import { UserService } from '../../../../EntityModules/User/Users.service';
import { USER_ROLE } from '../../../../EntityModules/User/User.model';

@Component({
    selector: 'login-component',
    templateUrl: './Login.component.html',
    styleUrls: ['./Login.component.less']
})

export class LoginComponent extends BaseComponent implements AfterViewInit {

  @ViewChild('loader') private loader : Loader;
  @ViewChild('authModal') private authModal : Modal;

  form : FormGroup;

  error = false;

  constructor(
    public router: Router,
    private authorizationService : AuthorizationService,
    private validatorService : ValidatorService,
    private userService : UserService
  ) {
    super();

    this.form = new FormGroup({
      username : new FormControl(''),
      password : new FormControl('')
    });
  }

  ngAfterViewInit() {
    this.authModal.open();
  }

  login() {

    this.loader.show();
    this.error = false;
    this.authorizationService
      .login(this.form.value.username, this.form.value.password, this.form.value.rememberMe)
      .subscribe(res => {

        this.onSuccess();

      }, err => {
        this.loader.hide();
        this.error = true;
        this.validatorService.addErrorToForm(this.form, err);
      });

  }

  onSuccess() {
    this.userService.getProfile().subscribe(res => {

      if(res.role === USER_ROLE.ROLE_REGISTRAR) {
        this.router.navigate(['/registrar/dashboard', {outlets : {auth : null}}]);
      } else {
        this.router.navigate(['/profile', {outlets : {auth : null}}]);
      }
      this.loader.hide();
    }, err => {
      this.loader.hide();
    });

  }

  onClose() {
    this.router.navigate([{ outlets: { auth: null }}]);
  }

  showForgotPopup() {
    this.router.navigate(['/', {outlets : {auth : ['forgot']}}]);
  }

}
