import { AfterViewInit, Component, ViewChild } from '@angular/core';
import {BaseComponent} from "../../../../BaseClasses/Components/Base.component";
import { Modal } from '../../../../BaseModules/Modal/Components/Modal/Modal.component';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../../../BaseModules/Authorization/Authorization.service';
import { Loader } from '../../../../BaseModules/Loader/Loader.component';
import { FormControl, FormGroup } from '@angular/forms';
import { ValidatorService } from '../../../../BaseModules/Validation/Validation.service';

@Component({
    selector: 'forgot-component',
    templateUrl: './Forgot.component.html',
    styleUrls: ['./Forgot.component.less']
})

export class ForgotComponent extends BaseComponent implements AfterViewInit {

  @ViewChild('authModal') private authModal : Modal;
  @ViewChild('loader') private loader : Loader;

  form : FormGroup;

  success = false;

  constructor(
    public router: Router,
    private authorizationService : AuthorizationService,
    private validatorService : ValidatorService
  ) {
    super();
    this.form = new FormGroup({
      login : new FormControl('')
    });
  }

  ngAfterViewInit() {
    this.authModal.open();
  }

  submit() {

    if(!this.form.value.login) {
      return;
    }

    this.loader.show();
    this.authorizationService.forgotPass(this.form.value.login).subscribe(() => {
      this.loader.hide();
      this.success = true;
    }, err => {
      this.loader.hide();
      this.validatorService.addErrorToForm(this.form, err);
    })
  }

  onClose() {
    this.router.navigate([{ outlets: { auth: null }}]);
  }

}
