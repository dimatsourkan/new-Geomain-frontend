import { AfterViewInit, Component, ViewChild } from '@angular/core';
import {BaseComponent} from "../../../../BaseClasses/Components/Base.component";
import { Modal } from '../../../../BaseModules/Modal/Components/Modal/Modal.component';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorizationService } from '../../../../BaseModules/Authorization/Authorization.service';
import { Loader } from '../../../../BaseModules/Loader/Loader.component';
import { FormControl, FormGroup } from '@angular/forms';
import { ValidatorService } from '../../../../BaseModules/Validation/Validation.service';

@Component({
    selector: 'change-pass-component',
    templateUrl: './ChangePass.component.html',
    styleUrls: ['./ChangePass.component.less']
})

export class ChangePassComponent extends BaseComponent implements AfterViewInit {

  @ViewChild('authModal') private authModal : Modal;
  @ViewChild('loader') private loader : Loader;

  token : string;
  form : FormGroup;

  success = false;

  constructor(
    public router: Router,
    private authorizationService : AuthorizationService,
    private ActivatedRoute : ActivatedRoute,
    private validatorService : ValidatorService
  ) {
    super();
    this.form = new FormGroup({
      password : new FormControl(''),
      token    : new FormControl(this.ActivatedRoute.snapshot.params['token'])
    });
  }

  ngAfterViewInit() {
    this.authModal.open();
  }

  submit() {

    if(!this.form.value.password) {
      return;
    }

    this.loader.show();
    this.authorizationService.changePass(this.form.value.password, this.form.value.token).subscribe(() => {
      this.loader.hide();
      this.success = true;
    }, err => {
      this.loader.hide();
      this.validatorService.addErrorToForm(this.form, err);
    })
  }

  onClose() {
    this.router.navigate([{ outlets: { auth: null }}]);
  }

}
