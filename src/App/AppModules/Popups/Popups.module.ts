import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from '../../BaseModules/Modal/Modal.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForgotComponent } from './Components/Forgot/Forgot.component';
import { LoginComponent } from './Components/Login/Login.component';
import { FormControlsModule } from '../../BaseModules/FormControls/FormControls.module';
import { LoaderModule } from '../../BaseModules/Loader/Loader.module';
import { ValidationModule } from '../../BaseModules/Validation/Validation.module';
import { ChangePassComponent } from './Components/ChangePass/ChangePass.component';



@NgModule({
  imports : [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    ModalModule,
    LoaderModule,
    ValidationModule,
    FormControlsModule
  ],
  declarations : [
    LoginComponent,
    ForgotComponent,
    ChangePassComponent
  ],
  exports : [
    LoginComponent,
    ForgotComponent,
    ChangePassComponent
  ],
  providers : []
})

export class PopupsModule {
}
