import {SearchComponent} from "./Search.component";
import {NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';
import { LoaderModule } from '../../BaseModules/Loader/Loader.module';

@NgModule({
    imports: [
      CommonModule,
      LoaderModule
    ],
    declarations: [
        SearchComponent
    ],
    exports : [
        SearchComponent
    ],
    providers: []
})

export class SearchModule {}
