import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { Helpers, IHelpers } from '../../BaseClasses/Helpers';
import { GeomainService } from '../../EntityModules/Geomain/Geomain.service';
import { Loader } from '../../BaseModules/Loader/Loader.component';



@Component({
  selector : 'search',
  templateUrl : './Search.component.html',
  styleUrls : ['./Search.component.less']
})

export class SearchComponent {
  helpers : IHelpers = Helpers;

  @ViewChild('search') private search : ElementRef;
  @ViewChild('loader') private loader : Loader;

  @Output() OnSuccess : EventEmitter<any> = new EventEmitter<any>();

  error : boolean = false;

  constructor(private geoNumberService : GeomainService) {

  }

  get value() {
    return this.search.nativeElement.value.replace(/-/g, '');
  }

  find() {
    if(!this.value) {
      return;
    }
    this.error = false;
    this.loader.show();
    this.geoNumberService.find(this.value).subscribe((res : any) => {
      this.loader.hide();
      this.OnSuccess.emit({
        value  : this.value,
        result : res
      });
    }, () => {
      this.loader.hide();
      this.error = true;
    });
  }
}
