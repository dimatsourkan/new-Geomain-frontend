import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ROUTING } from './app.routing';
import { TokenService } from './BaseClasses/Services/Token.service';
import { LoaderModule } from './BaseModules/Loader/Loader.module';
import { CommonModule } from '@angular/common';
import { HomeModule } from './AppModules/PageModules/Home/Home.module';
import { WrapperComponent } from './Components/Wrapper/wrapper.component';
import { HeaderComponent } from './Components/Header/Header.component';
import { FooterComponent } from './Components/Footer/Footer.component';
import { ModalModule } from './BaseModules/Modal/Modal.module';
import { NotificationsModule } from './BaseModules/Notifications/Notifications.module';
import { CanActivateApp } from './app.guard';
import { PopupsModule } from './AppModules/Popups/Popups.module';
import { HttpClientModule } from '@angular/common/http';
import { AppHttpModule } from './BaseModules/Http/Http.module';
import { WrapperLightComponent } from './Components/WrapperLight/wrapper-light.component';



@NgModule({
  imports : [

    CommonModule,
    BrowserModule,
    HttpClientModule,
    AppHttpModule,

    NotificationsModule,
    LoaderModule,
    ModalModule,
    PopupsModule,

    HomeModule,

    ROUTING

  ],

  declarations : [
    AppComponent,
    WrapperComponent,
    FooterComponent,
    HeaderComponent,
    WrapperLightComponent
  ],

  providers : [
    TokenService,
    CanActivateApp
  ],

  bootstrap : [
    AppComponent
  ]
})

export class AppModule {

  constructor() {

  }
}